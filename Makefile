all: build
BLUE=\033[01;34m
GREEN=\033[01;32m
CYAN=\033[01;36m
RED=\033[01;31m
MAGENTA=\033[01;35m
YELLOW=\033[01;33m
WHITE=\033[01;37m

build:
	@cd src/ && make
	@cd ..

clean:
	@cd src/ && make clean
	@cd ..

upload:
	@cd src/ && make upload
	@cd ..
pdf:
	@echo -e "${GREEN}Generating PDF from LaTeX...${WHITE}"
	@cd tex && make

depend:
	@cd src/ && make depend
	@cd ..

