/*
Copyright 2008  Lukas Kropatschek lukas.krop@gmail.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of 
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "PS2.h"
#include <pinDefines.h>

byte curBuffer;
byte bufferPos;
byte completeBuffer;
byte breakActive;
byte ignore;
byte run;

ISR(INT1_vect)
{
	// read bit from 
	byte in = getPinSt(ps2DataPin);

	// read the bit in the buffer
	// only if we are past the start bit
	// and not behind the stop bit
	if(bufferPos > 0 && bufferPos < 9 )
	{
		// read into buffer
		curBuffer |= (in << (bufferPos - 1));
	}

	// increment buffer position
	bufferPos++;

	// if we are behind the stop bit
	if(bufferPos == 11)
	{
		if(curBuffer == 0xF0)
			breakActive = 1;
		else if(breakActive == 1)
			breakActive = 0;
		else
			// set the complete buffer
			completeBuffer = curBuffer;

		// clear current buffer
		curBuffer = 0;
		// set buffer postion to 0 again
		bufferPos = 0;
	}

}

PS2::PS2()
{
	bufferPos = 0;
	curBuffer = 0;
	completeBuffer = 0;
	breakActive = 0;
	ignore = 0;
}

void PS2::begin()
{
	// Idle state of the PS/2 protocl - clk and data HIGH
	goIdle();

	// Enable INT1 interrupt
	EIMSK |= (1 << INT1);
	// Falling edge triggers interrupt
	EICRA |= (0 << ISC10) | (1 << ISC11);

}

/*void PS2::write(byte data)
{
	byte parity = 1;

	// set clk HIGH and data LOW - enter request-to-send mode
	goCommand();

	setPin(ps2DataPin, HIGH);
	setPin(ps2ClkPin, HIGH);
	delayMicroseconds(300);
	setPin(ps2ClkPin, LOW);
	delayMicroseconds(300);
	setPin(ps2DataPin, LOW);
	delayMicroseconds(10);

	// start bit
	setPin(ps2ClkPin, HIGH);

	// wait for device to take control of the clock signal
	while(getPinSt(ps2ClkPin) == HIGH);

	// send data
	for(byte i = 0; i < 8; i++)
	{
		if((data & 0x01) == 1)
			setPin(ps2DataPin, HIGH);
		else
			setPin(ps2DataPin, LOW);

		while(getPinSt(ps2ClkPin) == LOW);
		while(getPinSt(ps2ClkPin) == HIGH);
		parity = parity ^ (data & 0x01);
		data = data >> 1;
	}

	if(parity)
		setPin(ps2DataPin, HIGH);
	else
		setPin(ps2DataPin, LOW);

	while(getPinSt(ps2ClkPin) == LOW);
	while(getPinSt(ps2ClkPin) == HIGH);

	setPin(ps2DataPin, HIGH);
	delayMicroseconds(50);
	while(getPinSt(ps2ClkPin) == HIGH);

	while((getPinSt(ps2ClkPin) == LOW) || (getPinSt(ps2DataPin) == LOW));

	setPin(ps2ClockPin)

}*/

byte PS2::read()
{
	if(completeBuffer != 0 && breakActive != 1 && ignore != 1)
	{
		byte ret = completeBuffer;
		completeBuffer = 0;
		return ret;
	}
	else
		return 0;
}

byte PS2::available()
{
	if(completeBuffer != 0 && breakActive != 1)
		return 1;
	else
		return 0;
}

void PS2::goIdle()
{
	pinMode(ps2ClkPin, INPUT);
	pinMode(ps2DataPin, INPUT);
	setPin(ps2ClkPin, HIGH);
	setPin(ps2DataPin, HIGH);
}

void PS2::goCommand()
{
	pinMode(ps2ClkPin, INPUT);
	pinMode(ps2DataPin, OUTPUT);
	setPin(ps2ClkPin, HIGH);
	setPin(ps2DataPin, LOW);
}

PS2 kbd = PS2();

