/*
Copyright 2008  Lukas Kropatschek lukas.krop@gmail.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PS2_H
#define PS2_H

#include <arduino.h>

/**
 * \brief Class for interfacing with an PS/2 device
 */

class PS2
{

	public:
		/**
		 * \brief CTOR
		 */
		PS2();

		//void write(byte data);
		/**
		 * \brief Read data from the PS/2 device
		 * \return data read from the PS/2 device
		 */
		byte read();

		byte available();

		void begin();

	private:
		void goIdle();
		void goCommand();
};

extern PS2 kbd;

#endif

