/*
Copyright 2008  Lukas Kropatschek <lukas.krop@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of 
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Stepper.h"

Stepper::Stepper(unsigned int numSteps, byte pin1, byte pin2)
{
	// current step of the motor
	currentStep = 0;
	// speed, in revolutions per minute
	curSpeed = 0;
	// drive direction
	direction = 0;
	// time stamp in ms of the last step
	lastStepTime = 0;
	// total number of steps of the motor
	maxSteps = numSteps;

	// Pins on the arduino the motor is using
	motorPin1 = pin1;
	motorPin2 = pin2;

	// set the pins to output
	pinMode(motorPin1, OUTPUT);
	pinMode(motorPin2, OUTPUT);

}

void Stepper::setSpeed(long speed)
{
	curSpeed = speed;
	// delay betwwen steps, revoulutions per minute
	stepDelay = (60L * 1000L / maxSteps / speed);
}

#ifdef _DEBUG_
byte Stepper::getSpeed()
{
	return curSpeed;
}
#endif

void Stepper::step(int steps)
{
	// steps left in this function call
	int stepsLeft = abs(steps);

	// if steps is positive -> drive direction forward
	if(steps > 0)
			direction = 1;
	// if steps is negative -> drive direction backwards
	if(steps < 0)
			direction = 0;

	// as long as there are steps left
	while(stepsLeft > 0)
	{
		// move only if the appropriate delay has passed
		if((millis() - lastStepTime) >= stepDelay)
		{
			// step the motor to phase 1, 2, 3 or 4
			stepMotor(currentStep % 4);
			// get the timestap
			lastStepTime = millis();

			// depending on direction
			if(direction == 1)
			{
				// increment the current step
				currentStep++;
				if(currentStep == maxSteps)
					currentStep = 0;
			}

			else 
			{
				if(currentStep == 0)
					currentStep = maxSteps;

				// decrement steps
				currentStep--;
			}
			// decrement steps left
			stepsLeft--;
		}
	}

}

void Stepper::stepMotor(byte thisStep)
{
	switch (thisStep)
	{
		case 0:
		setPin(motorPin1, LOW);
		setPin(motorPin2, HIGH);
		break;

		case 1:
		setPin(motorPin1, HIGH);
		setPin(motorPin2, HIGH);
		break;

		case 2:
		setPin(motorPin1, HIGH);
		setPin(motorPin2, LOW);
		break;

		case 3:
		setPin(motorPin1, LOW);
		setPin(motorPin2, LOW);
		break;
	}
}

