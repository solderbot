/*Copyright 2008  Lukas Kropatschek <lukas.krop@gmail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of 
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef STEPPER_H
#define STEPPER_H

#include <arduino.h>

/**
 * \brief Class for interfacing with a stepper motor over 2 wires
 */
class Stepper 
{
	public:
		/**
		 * \brief CTOR
		 * \param numberOfSteps Maximum number of steps the motor can do
		 * \param pin1 First motor pin
		 * \param pin2 Second motor pin
		 */
		Stepper(unsigned int numberOfSteps, byte pin1, byte pin2);

		/**
		 * \brief Set the speed for stepping the motor
		 * \param speed the speed
		 */
		void setSpeed(long speed);
		
		/**
		 * \brief Make a/some steps
		 * \param numberOfSteps The number of steps to step
		 * Negative number means backwards, positive forwards.
		 * Please note, that this function is blocking which means
		 * you have to wait until all steps are done until the next
		 * step in code.
		 */
		void step(int numberOfSteps);
		
		int maxSteps;
		
		#ifdef _DEBUG_
		byte getSpeed();
		#endif
		
	private:
		void stepMotor(byte step);

		byte direction;
		byte curSpeed;
		unsigned long stepDelay;
		int currentStep;
		long lastStepTime;

		byte motorPin1;
		byte motorPin2;
};

#endif

