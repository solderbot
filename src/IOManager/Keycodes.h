/*
Copyright 2008  Lukas Kropatschek lukas.krop@gmail.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of 
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <avr/pgmspace.h>

#include <core/defines.h>

prog_uchar KeycodesToChars[10][2] PROGMEM = {
	{KEYCODE_0, '0'},
	{KEYCODE_1, '1'},
	{KEYCODE_2, '2'},
	{KEYCODE_3, '3'},
	{KEYCODE_4, '4'},
	{KEYCODE_5, '5'},
	{KEYCODE_6, '6'},
	{KEYCODE_7, '7'},
	{KEYCODE_8, '8'},
	{KEYCODE_9, '9'}
};

enum SBUI_KEYS {
	KEY_0 = 0,
	KEY_1,
	KEY_2,
	KEY_3,
	KEY_4,
	KEY_5,
	KEY_6,
	KEY_7,
	KEY_8,
	KEY_9,
	KEY_COUNT
};

/*prog_uchar KeycodesToChars[43][2] PROGMEM = {
	{KEYCODE_A, 'a'},
	{KEYCODE_B, 'b'},
	{KEYCODE_C, 'c'},
	{KEYCODE_D, 'd'},
	{KEYCODE_E, 'e'},
	{KEYCODE_F, 'f'},
	{KEYCODE_G, 'g'},
	{KEYCODE_H, 'h'},
	{KEYCODE_I, 'i'},
	{KEYCODE_J, 'j'},
	{KEYCODE_K, 'k'},
	{KEYCODE_L, 'l'},
	{KEYCODE_M, 'm'},
	{KEYCODE_N, 'n'},
	{KEYCODE_O, 'o'},
	{KEYCODE_P, 'p'},
	{KEYCODE_Q, 'q'},
	{KEYCODE_R, 'r'},
	{KEYCODE_S, 's'},
	{KEYCODE_T, 't'},
	{KEYCODE_U, 'u'},
	{KEYCODE_V, 'v'},
	{KEYCODE_W, 'w'},
	{KEYCODE_X, 'x'},
	{KEYCODE_Y, 'y'},
	{KEYCODE_Z, 'z'},
	{KEYCODE_1, '1'},
	{KEYCODE_2, '2'},
	{KEYCODE_3, '3'},
	{KEYCODE_4, '4'},
	{KEYCODE_5, '5'},
	{KEYCODE_6, '6'},
	{KEYCODE_7, '7'},
	{KEYCODE_8, '8'},
	{KEYCODE_9, '9'},
	{KEYCODE_0, '0'},
	{KEYCODE_MINUS, '-'},       
	{KEYCODE_PLUS, '+'},       
	{KEYCODE_QUOTE, '`'},
	{KEYCODE_COMMA, ','},
	{KEYCODE_DOT, '.'},
	{KEYCODE_CONSOLE, '^'},
	{KEYCODE_ANGLE_BRACKET_LEFT, '<'}
};

enum SBGT_KEYS {
		KEY_A = 0,
		KEY_B,
		KEY_C,
		KEY_D,
		KEY_E,
		KEY_F,
		KEY_G,
		KEY_H,
		KEY_I,
		KEY_J,
		KEY_K,
		KEY_L,
		KEY_M,
		KEY_N,
		KEY_O,
		KEY_P,
		KEY_Q,
		KEY_R,
		KEY_S,
		KEY_T,
		KEY_U,
		KEY_V,
		KEY_W,
		KEY_X,
		KEY_Y,
		KEY_Z,
		KEY_1,
		KEY_2,
		KEY_3,
		KEY_4,
		KEY_5,
		KEY_6,
		KEY_7,
		KEY_8,
		KEY_9,
		KEY_0,
		KEY_MINUS, 
		KEY_PLUS,
		KEY_QUOTE,
		KEY_COMMA,
		KEY_DOT,
		KEY_CONSOLE,
		KEY_ANGLE_BRACKET_LEFT,
		KEY_COUNT
};
*/
