/*
Copyright 2008  Lukas Kropatschek lukas.krop@gmail.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "IOManager.h"
#include <Serial.h>
#include <PS2.h>
#include <SBUI.h>
#include <SolderManager.h>
#include <core/defines.h>

#include "Keycodes.h"
#include "commands.h"

IOManager::IOManager()
{}

void IOManager::checkInput()
{
	// see if there is data on the serial line
	if (Serial.available() > 0)
	{
		// read data and parse it to do something
		byte in = Serial.read();
		parseSerialIn(in);
		return;
	}

	else
		if (kbd.available() == 1)
		{
			// read a keycode
			byte in = kbd.read(); // keycode
			// parse keycode and do something
			parsePS2In(in);
			return;
		}

		else
			return;
}

void IOManager::parsePS2In(byte in)
{
	// first test against movement keys

	switch (in)
	{

		case KEYCODE_F1:
			sbui.draw();
			break;

		case KEYCODE_F2:
			sbui.drawShowTrack();
			break;

		case KEYCODE_F3:
			sbui.drawLoadTrack(0, 0, 0);
			break;
			
		case KEYCODE_F4:
			sbui.drawCoordInput();
			break;
			
		case KEYCODE_LEFT:
			SolderMgr.manualStep(0, SB_UNIT_X);
			SolderMgr.curX += 1;
			sbui.update();
			break;

		case KEYCODE_RIGHT:
			SolderMgr.manualStep(0, -SB_UNIT_X);
			SolderMgr.curX -= 1;
			sbui.update();
			break;

		case KEYCODE_UP:
			SolderMgr.manualStep(1, SB_UNIT_Y);
			SolderMgr.curY += 1;
			sbui.update();
			break;

		case KEYCODE_DOWN:
			SolderMgr.manualStep(1, -SB_UNIT_Y);
			SolderMgr.curY -= 1;
			sbui.update();
			break;

		case KEYCODE_S:
			/*SolderMgr.manualStep(2, 200);
			delay(1000);
			SolderMgr.manualStep(2, -200);*/
			SolderMgr.solder();
			break;
	}
}

byte IOManager::getPS2Keycode()
{
	if (kbd.available() == 1)
	{
		// read a keycode
		byte keycode  = kbd.read(); // keycode
		byte n, test, character;

		// Check against the KeycodesToChar array until we found
		// aproppriate ASCII character

		if (keycode == KEYCODE_ENTER)
			return keycode;

		if (keycode == KEYCODE_BACKSPACE)
			return keycode;

		if (keycode == KEYCODE_TAB)
			return keycode;
			
		if (keycode == KEYCODE_SPACE)
			return keycode;
			
		if (keycode == KEYCODE_V)
			return keycode;

		for (n = 0; n < KEY_COUNT; n++)
		{
			test = pgm_read_byte_near(&KeycodesToChars[n][0]);

			if (test == keycode)
				break;
		}

		// Read the character out of program memory area
		character = pgm_read_byte_near(&KeycodesToChars[n][1]);

		// write the character into the buffer
		return character;
	}

	return 0;
}

void IOManager::parseSerialIn(byte in)
{
	if (in == BEGIN_SMAP)
		readSMap();
}

void IOManager::readSMap()
{
	byte pos = 0;
	byte n = 0;

	// as long as there is data coming in

	while (true)
	{
		if (Serial.available() > 0)
		{
			// read in a byte
			byte in = Serial.read();

			switch (in)
			{
					// TOTAL command

				case SMAP_TOTAL:
					// read the total number of
					// coord points

					while (true)
					{
						byte total = 0;

						if (Serial.available() > 0)
							total = Serial.read();

						// set total number
						sbui.total = total;

						if (total != 0)
							break;
					}

					break;

					// set pos to 1 to read a
					// x coord value

				case SMAP_X:
					pos = 1;
					break;
					// set pos to 2 to read
					// a y coord value

				case SMAP_Y:
					pos = 2;
					break;
					// set pos to 3 to read
					// the current # of coord

				case SMAP_COUNT:
					pos = 3;
					break;
					// transaction complete

				case END_SMAP:
					// draw buttons to show and reread
					sbui.drawLoadTrack(101, 0, 0);
					pos = 4;
					break;

				default:

					if (pos == 3)
					{
						// increment/set the current count
						n = in;
						break;
					}

					else
						if (pos == 1)
						{
							// set the x coord
							smap[n][pos] = in;
							// update the LoadTrack view
							sbui.drawLoadTrack(n, in, pos);
							break;
						}

						else
							if (pos == 2)
							{
								// set the y coord
								smap[n][pos] = in;
								// update the LoadTrack view
								sbui.drawLoadTrack(n, in, pos);
								break;
							}
			}
		}

		// break out of the
		// endless loop if the
		// transaction is complete

		if (pos == 4)
			break;
	}
}

IOManager IOMgr = IOManager();

