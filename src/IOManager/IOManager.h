/*
Copyright 2008  Lukas Kropatschek lukas.krop@gmail.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of 
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef IOMANAGER_H
#define IOMANAGER_H

#include <arduino.h>

/**
  * \brief Input/Output manager
  * This class handles all input and output
  */

class IOManager
{
public:

	// Constructors/Destructors
	/**
	 * \brief Constructor
	 */
	IOManager();

	void checkInput();
	
	/**
	 * \brief Wait for serial input, parse and do appropriate action
	 */
// 	void serialInput();

	/**
	 * \brief Send data out via serial line
	 * \param  out data to be send out
	 */
// 	void serialOutput(byte out );

	/**
	 * \brief Wait for input from a PS/2 device, parse and do approptiate action
	 */
// 	void ps2Input();

	/**
	 * \brief Returns the text which did come in via PS/2
	 * \returns one ASCII character
	 */
	byte getPS2Keycode();

	/**
	 * \brief Reads a SMAP into the smap array
	 */
	void readSMap();

	byte smap[100][2];

	//byte smapReady;

private:
	void parsePS2In(byte in);
	void parseSerialIn(byte in);

	byte lastSerIn;

	/**
	 * \brief Parses a command and calls the appropriate function
	 */
//	void parseCmdAndWork(byte cmd );

//	byte buffer;
//	byte lastKeycode;
};

extern IOManager IOMgr;

#endif // IOMANAGER_H
