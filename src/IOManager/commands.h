/*
Copyright 2008  Lukas Kropatschek lukas.krop@gmail.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of 
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef COMMANDS_H
#define COMMANDS_H

#define GO_LEFT 0xA1
#define GO_RIGHT 0xA2
#define GO_UP 0xA3
#define GO_DOWN 0xA4
#define SWITCH_MAIN 0xA5
#define SWITCH_GRAPH 0xA6
#define SWITCH_LOAD 0xA7
#define ENTER 0xA9
// smap commands
#define BEGIN_SMAP 0xAA
#define SMAP_X 0xAB
#define SMAP_Y 0xAC
#define END_SMAP 0xAD
#define SMAP_COUNT 0xAE
#define SMAP_TOTAL 0xAF

#endif

