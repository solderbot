/*
Copyright 2008  Lukas Kropatschek lukas.krop@gmail.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of 
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef SOLDERMANAGER_H
#define SOLDERMANAGER_H

/**
  * class SolderManager
  * 
  */

#include <arduino.h>

class SolderManager
{
public:

	/**
	 * Empty Constructor
	 */
	SolderManager();

	/**
	 * \brief Goes with the solder arms to the specified position
	 * \param x x coord
	 * \param y y coord
	 */
	void goTo(byte x, byte y );

	/**
	 * \brief Solders
	 */
	void solder();

	/**
	 * \brief Manual Stepper control
	 */
	void manualStep(byte stepper, int steps);

	byte curX;
	byte curY;
	
	#ifdef _DEBUG_
	void setMaxSteps(byte motor, unsigned int steps);
	unsigned int getMaxSteps(byte motor);
	void setSpeed(byte motor, byte speed);
	unsigned long getSpeed(byte motor);
	#endif

};

extern SolderManager SolderMgr;

#endif // SOLDERMANAGER_H
