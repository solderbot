/*
Copyright 2008  Lukas Kropatschek lukas.krop@gmail.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "SolderManager.h"
#include <Stepper.h>

// SB_UNIT
#include <core/defines.h>

#include <SBUI.h>

#define XORIGIN (80 * SB_UNIT)
#define YORIGIN ()

Stepper stepperX(100, 8, 9);
Stepper stepperY(100, 10, 11);
Stepper stepperZ(100, 12, 13);

SolderManager::SolderManager()
{
	stepperX.setSpeed(150);
	stepperY.setSpeed(150);
	stepperZ.setSpeed(150);

	curX = 0;
	curY = 0;
}

void SolderManager::goTo(byte x, byte y)
{
	signed int xi, yi;
	byte xdir, ydir, xabs, yabs;

	xdir = 0;
	ydir = 0;

	// calculate the distance we have to step
	xi = x - curX;
	yi = y - curY;

	//check which direction we have to drive

	if (xi < 0)
	{
		xdir = 1;
	}

	if (yi < 0)
	{
		ydir = 1;
	}

	// get absoult value of the steps
	xabs = abs(xi);

	yabs = abs(yi);

	if (xdir == 0 && ydir == 0)
	{
		while (xi != 0)
		{
			stepperX.step(SB_UNIT_X);
			xi--;
		}

		while (yi != 0)
		{
			stepperY.step(SB_UNIT_Y);
			yi--;
		}
	}

	else
		if (xdir == 1 && ydir == 1)
		{
			while (xi != 0)
			{
				stepperX.step(-SB_UNIT_X);
				xi++;
			}

			while (yi != 0)
			{
				stepperY.step(-SB_UNIT_Y);
				yi++;
			}
		}

		else
			if (xdir == 1 && ydir == 0)
			{
				while (xi != 0)
				{
					stepperX.step(-SB_UNIT_X);
					xi++;
				}

				while (yi != 0)
				{
					stepperY.step(SB_UNIT_Y);
					yi--;
				}
			}

			else
				if (xdir == 0 && ydir == 1)
				{
					while (xi != 0)
					{
						stepperX.step(SB_UNIT_X);
						xi--;
					}

					while (yi != 0)
					{
						stepperY.step(-SB_UNIT_Y);
						yi++;
					}
				}

	curX = x;

	curY = y;
	sbui.update();
}

void SolderManager::solder()
{
	//TODO Check unit
	manualStep(2, 500);
	delay(1000);
	manualStep(2, -500);
}

void SolderManager::manualStep(byte stepper, int steps)
{
	switch (stepper)
	{

		case 0:
			stepperX.step(steps);
			break;

		case 1:
			stepperY.step(steps);
			break;

		case 2:
			stepperZ.step(steps);
			break;

		default:
			break;
	}
}

#ifdef _DEBUG_
void SolderManager::setMaxSteps(byte motor, unsigned int steps)
{
	switch (motor)
	{

		case 0:
			stepperX.maxSteps = steps;
			break;

		case 1:
			stepperY.maxSteps = steps;
			break;

		case 2:
			stepperZ.maxSteps = steps;
			break;
	}
}

unsigned int SolderManager::getMaxSteps(byte motor)
{
	switch (motor)
	{

		case 0:
			return stepperX.maxSteps;
			break;

		case 1:
			return stepperY.maxSteps;
			break;

		case 2:
			return stepperZ.maxSteps;
			break;
	}

	return 0;
}


void SolderManager::setSpeed(byte motor, byte speed)
{
	switch (motor)
	{

		case 0:
			stepperX.setSpeed(speed);
			break;

		case 1:
			stepperY.setSpeed(speed);
			break;

		case 2:
			stepperZ.setSpeed(speed);
			break;
	}
}

unsigned long SolderManager::getSpeed(byte motor)
{
	switch (motor)
	{

		case 0:
			return stepperX.getSpeed();
			break;

		case 1:
			return stepperY.getSpeed();
			break;

		case 2:
			return stepperZ.getSpeed();
			break;
	}

	return 0;
}

#endif

SolderManager SolderMgr = SolderManager();

