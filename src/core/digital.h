/*
Copyright 2008  Lukas Kropatschek lukas.krop@gmail.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of 
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DIGITALIO_H
#define DIGITALIO_H

#include "defines.h"

/**
 * \brief Returns the appropriate port to a pin
 * \param pin the pin
 * \return The appropriate port to the pin
 */
byte getPortFromPin(byte pin);
/**
 * \brief Set the mode of a pin to input or output
 * \param pin the pin
 * \param mode the mode, could be INPUT or OUTPUT.
 */
void pinMode(byte pin, byte mode);

/**
 * \brief Sets the status of a pin to HIGH or LOW
 * \param pin the pin
 * \param mode the mode, could be HIGH or LOW
 */
void setPin(byte pin, byte mode);

/**
 * \brief Returns the current status of a digital pin
 * \param pin the pin
 * \return the status of the pin, could be HIGH or LOW
 */
byte getPinSt(byte pin);

byte getPinTimer(byte pin);

inline void turnOffPWM(byte timer);

#endif

