/*
Copyright 2008  Lukas Kropatschek lukas.krop@gmail.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of 
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CORE_H
#define CORE_H

#include <avr/io.h>
#include <avr/interrupt.h>
/**
 * \brief Basic Initializations
 * Needs to be called before setup() and loop()
 */
void init(void);
/**
 * \brief Setup
 * Setup your code here before entering the loop
 */
void setup(void);
/**
 * \brief Main application loop
 */
void loop(void);
/**
 * \brief Returns the milliseconds since the program has started
 * \return milliseconds since program started
 */
unsigned long millis(void);
/**
 * \brief Returns the time since the program has started
 * \return uptime since program started
 */
void delay(unsigned long del);
/**
 * \brief Pauses the application
 * \param us For how long the application should be paused in microseconds
 */
void delayMicroseconds(unsigned int us);
/**
 * \brief Returns available SRAM
 * \return Free SRAM
 */
int availableSRAM();

#endif

