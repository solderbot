/*
Copyright 2008  Lukas Kropatschek lukas.krop@gmail.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of 
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef DEFINES_H
#define DEFINES_H

/**
 * \brief set the bit BIT in Register R to 1
 */
#define setBit(R,BIT) ( (R) |= (1 << (BIT)) ) 
/**
 * \brief clear the bit BIT in Register R (set to 0)
 */
#define clearBit(R,BIT) ( (R) &= ~(1 << (BIT)) )
/**
 * \brief unsigned char equals one byte
 */
typedef unsigned char byte;

#define LOW 0
#define HIGH 1
#define OUTPUT 2
#define INPUT 3

#define SB_UNIT_X 62
#define SB_UNIT_Y 146

#define KEYCODE_A 0x1C 
#define KEYCODE_B 0x32
#define KEYCODE_C 0x21
#define KEYCODE_D 0x23
#define KEYCODE_E 0x24
#define KEYCODE_F 0x2B
#define KEYCODE_G 0x34
#define KEYCODE_H 0x33
#define KEYCODE_I 0x43
#define KEYCODE_J 0x3B
#define KEYCODE_K 0x42
#define KEYCODE_L 0x4B
#define KEYCODE_M 0x3A
#define KEYCODE_N 0x31
#define KEYCODE_O 0x44
#define KEYCODE_P 0x4D
#define KEYCODE_Q 0x15
#define KEYCODE_R 0x2D
#define KEYCODE_S 0x1B
#define KEYCODE_T 0x2C
#define KEYCODE_U 0x3C
#define KEYCODE_V 0x2A
#define KEYCODE_W 0x1D
#define KEYCODE_X 0x22
#define KEYCODE_Y 0x1A
#define KEYCODE_Z 0x35
#define KEYCODE_1 0x16
#define KEYCODE_2 0x1E
#define KEYCODE_3 0x26
#define KEYCODE_4 0x25
#define KEYCODE_5 0x2E
#define KEYCODE_6 0x36
#define KEYCODE_7 0x3D
#define KEYCODE_8 0x3E
#define KEYCODE_9 0x46
#define KEYCODE_0 0x45
#define KEYCODE_MINUS 0x4A
#define KEYCODE_PLUS 0x5B
#define KEYCODE_QUOTE 0x55
#define KEYCODE_COMMA 0x41
#define KEYCODE_DOT 0x49
#define KEYCODE_LEFT 0x6B
#define KEYCODE_RIGHT 0x74
#define KEYCODE_UP 0x75
#define KEYCODE_DOWN 0x72
#define KEYCODE_TAB 0xD
#define KEYCODE_CONSOLE 0xE
#define KEYCODE_SHIFT 0x12
#define KEYCODE_ESCAPE 0x76
#define KEYCODE_ENTER 0x5A
#define KEYCODE_SPACE 0x29
#define KEYCODE_CONTROL 0x14
#define KEYCODE_ALT 0x11
#define KEYCODE_F1 0x5 
#define KEYCODE_F2 0x6
#define KEYCODE_F3 0x4
#define KEYCODE_F4 0xC
#define KEYCODE_F5 0x3
#define KEYCODE_F6 0xB
#define KEYCODE_F7 0x83
#define KEYCODE_F8 0xA
#define KEYCODE_F9 0x1
#define KEYCODE_F10 0x9
#define KEYCODE_F11 0x78
#define KEYCODE_F12 0x7
#define KEYCODE_DEL 0x71
#define KEYCODE_BACKSPACE 0x66
#define KEYCODE_ANGLE_BRACKET_LEFT 0x61
#define KEYCODE_RELEASED 0xF0
#define KEYCODE_RELEASED_ALT 0xE0

#endif

