 /*
Copyright 2008  Lukas Kropatschek lukas.krop@gmail.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of 
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "digital.h"
#include <avr/io.h>
#include <Serial.h>

#define PB 0xBB
#define PD 0xDD

#define NOTIMER 0
#define TIMER0A 1
#define TIMER0B 2
#define TIMER1A 3
#define TIMER1B 4
#define TIMER2A 6
#define TIMER2B 7

byte getPortFromPin(byte pin)
{
	if(pin < 8)
		return PD;
	else 
		return PB;

/*	byte port;
	switch(pin)
	{
		case 0:
			port = PD;
			break;
		case 1:
			port = PD;
			break;
		case 2:
			port = PD;
			break;
		case 3:
			port = PD;
			break;
		case 4:
			port = PD;
			break;
		case 5:
			port = PD;
			break;
		case 6:
			port = PD;
			break;
		case 7:
			port = PD;
			break;
		case 8:
			port = PB;
			break;
		case 9:
			port = PB;
			break;
		case 10:
			port = PB;
			break;
		case 11:
			port = PB;
			break;
		case 12:
			port = PB;
			break;
		case 13:
			port = PB;
			break;
		default:
			port = 0x00;
			break;
	}
	return port;
	*/
}

void pinMode(byte pin, byte mode)
{
	// get the appropirate port for the pin
	byte port = getPortFromPin(pin);

	if(port == PB)
	{
		// PORTB starts at 0 again
		pin = pin - 8;
		if(mode == OUTPUT)
			// setting a bit to 1 in the Drive Direction Register means output
			setBit(DDRB,pin);
		else if(mode == INPUT)
			// setting a bit to 0 in the Drive Direction Register means input
			clearBit(DDRB,pin);
	}

	// for PORTB the pin variable can stay as it is
	else if(port == PD)
	{
		if(mode == OUTPUT)
			setBit(DDRD,pin);
		else if(mode == INPUT)
			clearBit(DDRD,pin);
	}
}

void setPin(byte pin, byte mode)
{
	byte port = getPortFromPin(pin);

	if(port == PB)
	{
		pin = pin - 8;
		if(mode == HIGH)
			setBit(PORTB,pin);
		else if(mode == LOW)
			clearBit(PORTB,pin);
	}

	else if(port == PD)
	{
		if(mode == HIGH)
			setBit(PORTD,pin);
		else if(mode == LOW)
			clearBit(PORTD,pin);
	}
}

byte getPinSt(byte pin)
{	
	byte port = getPortFromPin(pin);
	byte status = 0x00;
	byte mask = 0x00;
	byte timer = getPinTimer(pin);

	if(port == PB)
	{
		pin = pin - 8;
		switch (pin)
		{
				case 0:
					mask = 0x1;
					break;
				case 1:
					mask = 0x2;
					break;
				case 2:
					mask = 0x4;
					break;
				case 3:
					mask = 0x8;
					break;
				case 4:
					mask = 0x10;
					break;
				case 5:
					mask = 0x20;
					break;
				case 6:
					mask = 0x40;
					break;
				case 7:
					mask = 0x80;
					break;
		}
		if(timer != NOTIMER)
			turnOffPWM(timer);
		status = (PINB & mask) >> pin;
		if(status == 0x1)
			return HIGH;
		else 
			return LOW;
	}

	else if(port == PD)
	{
		switch (pin)
		{
				case 0:
					mask = 0x1;
					break;
				case 1:
					mask = 0x2;
					break;
				case 2:
					mask = 0x4;
					break;
				case 3:
					mask = 0x8;
					break;
				case 4:
					mask = 0x10;
					break;
				case 5:
					mask = 0x20;
					break;
				case 6:
					mask = 0x40;
					break;
				case 7:
					mask = 0x80;
					break;
		}
		if(timer != NOTIMER)
			turnOffPWM(timer);
		status = (PIND & mask) >> pin;
		if(status == 0x1)
			return HIGH;
		else 
			return LOW;
	}

	else
		return LOW;
}

byte getPinTimer(byte pin)
{
	byte timer;

	switch(pin)
	{
		case 3:
			timer = TIMER2B;
			break;
		case 5:
			timer = TIMER0B;
			break;
		case 6:
			timer = TIMER0A;
			break;
		case 9:
			timer = TIMER1A;
			break;
		case 10:
			timer = TIMER1B;
			break;
		case 11:
			timer = TIMER2A;
			break;
		default:
			timer = NOTIMER;
			break;
	}
	return timer;
}

inline void turnOffPWM(byte timer)
{
	if (timer == TIMER1A) clearBit(TCCR1A, COM1A1);
	if (timer == TIMER1B) clearBit(TCCR1A, COM1B1);
	if (timer == TIMER0A) clearBit(TCCR0A, COM0A1);
    if (timer == TIMER0B) clearBit(TCCR0A, COM0B1);
    if (timer == TIMER2A) clearBit(TCCR2A, COM2A1);
    if (timer == TIMER2B) clearBit(TCCR2A, COM2B1);
}


