 /*
Copyright 2008  Lukas Kropatschek lukas.krop@gmail.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of 
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "core.h"
#include <stdlib.h>
#include "defines.h"

// The number of times timer 0 has overflowed since the program started.
// Must be volatile or gcc will optimize away some uses of it.
volatile unsigned long timer0OverflowCount;

ISR(TIMER0_OVF_vect)
{
	timer0OverflowCount++;
}

unsigned long millis()
{
	// timer 0 increments every 64 cycles, and overflows when it reaches
	// 256.  we would calculate the total number of clock cycles, then
	// divide by the number of clock cycles per millisecond, but this
	// overflows too often.
	//return timer0OverflowCount * 64UL * 256UL / (F_CPU / 1000UL);
	
	// instead find 1/128th the number of clock cycles and divide by
	// 1/128th the number of clock cycles per millisecond
	return timer0OverflowCount * 64UL * 2UL / (F_CPU / 128000UL);
}

void delay(unsigned long ms)
{
	unsigned long start = millis();
	
	while (millis() - start < ms);
}

/* Delay for the given number of microseconds.  Assumes a 16 MHz clock. 
 * Disables interrupts, which will disrupt the millis() function if used
 * too frequently. */
void delayMicroseconds(unsigned int us)
{
	uint8_t oldSREG;

	// calling avrlib's delay_us() function with low values (e.g. 1 or
	// 2 microseconds) gives delays longer than desired.
	//delay_us(us);

	// for a one-microsecond delay, simply return.  the overhead
	// of the function call yields a delay of approximately 1 1/8 us.
	if (--us == 0)
		return;

	// the following loop takes a quarter of a microsecond (4 cycles)
	// per iteration, so execute it four times for each microsecond of
	// delay requested.
	us <<= 2;

	// account for the time taken in the preceeding commands.
	us -= 2;

	// disable interrupts, otherwise the timer 0 overflow interrupt that
	// tracks milliseconds will make us delay longer than we want.
	oldSREG = SREG;
	cli();

	// busy wait
	__asm__ __volatile__ (
		"1: sbiw %0,1" "\n\t" // 2 cycles
		"brne 1b" : "=w" (us) : "0" (us) // 2 cycles
	);

	// reenable interrupts.
	SREG = oldSREG;
}

void init(void)
{
	// enable/allow all ISR (Interrupts)
	// this needs to be called before setup() or some functions won't
	// work there
	sei();
	
	// timer 0 is used for millis() and delay()
	timer0OverflowCount = 0;

	// on the ATmega168, timer 0 is also used for fast hardware pwm
	// (using phase-correct PWM would mean that timer 0 overflowed half as often
	// resulting in different millis() behavior on the ATmega8 and ATmega168)
	setBit(TCCR0A, WGM01);
	setBit(TCCR0A, WGM00);

	// set timer 0 prescale factor to 64
	setBit(TCCR0B, CS01);
	setBit(TCCR0B, CS00);
	
	// enable timer 0 overflow interrupt
	setBit(TIMSK0, TOIE0);

	// timers 1 and 2 are used for phase-correct hardware pwm
	// this is better for motors as it ensures an even waveform
	// note, however, that fast pwm mode can achieve a frequency of up
	// 8 MHz (with a 16 MHz clock) at 50% duty cycle

	// set timer 1 prescale factor to 64
	setBit(TCCR1B, CS11);
	setBit(TCCR1B, CS10);

	// put timer 1 in 8-bit phase correct pwm mode
	setBit(TCCR1A, WGM10);

	// set timer 2 prescale factor to 64
	setBit(TCCR2B, CS22);
	// configure timer 2 for phase correct pwm (8-bit)
	setBit(TCCR2A, WGM20);

	// set a2d reference to AVCC (5 volts)
	clearBit(ADMUX, REFS1);
	setBit(ADMUX, REFS0);

	// set anaolog to digital prescale factor to 128
	// 16 MHz / 128 = 125 KHz, inside the desired 50-200 KHz range.
	// XXX: this will not work properly for other clock speeds, and
	// this code should use F_CPU to determine the prescale factor.
	setBit(ADCSRA, ADPS2);
	setBit(ADCSRA, ADPS1);
	setBit(ADCSRA, ADPS0);

	// enable Analog to digital conversions
	setBit(ADCSRA, ADEN);

	// the bootloader connects pins 0 and 1 to the USART; disconnect them
	// here so they can be used as normal digital i/o; they will be
	// reconnected in Serial.begin()
	UCSR0B = 0;

	clearBit(MCUCR,PUD);

	// clear all bits in PORTD, PORTB, PORTC and DDRD, DDRB, DDRC
/*	PORTD = 0x00;
	DDRD = 0x00;
	PORTB = 0x00;
	DDRB = 0x00;
	PORTC = 0x00;
	DDRC = 0x00;*/

}

/*
int availableSRAM() 
{
  int size = 1024;
  byte *buf;

  while ((buf = (byte *) malloc(--size)) == NULL);

  free(buf);

  return size;
}
*/

extern int __bss_end;
extern void *__brkval;

int availableSRAM()
{
  int freeMemory;

  if((int)__brkval == 0)
    freeMemory = ((int)&freeMemory) - ((int)&__bss_end);
  else
    freeMemory = ((int)&freeMemory) - ((int)__brkval);

  return freeMemory;
}

