/*
Copyright 2008  Lukas Kropatschek lukas.krop@gmail.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of 
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "SButton.h"
#include <LCD.h>
//#include "roundedCorners.h"
#include "ButtonDefines.h"

SButton::SButton(const char* text, byte x, byte y, byte width, byte height)
{
	// initialize the attributes with the given paramters
	this->buttonText = text;
	this->x = x;
	this->y = y;
	this->width = width;
	this->height = height;
	// at creation time the button has no focus
	this->curFocus = NOFOCUS;
	this->lastFocus = NOFOCUS;
}

SButton::SButton()
{}

void SButton::draw()
{
	if(curFocus == HASFOCUS)
	{
		// define boundings
		byte x1,y1;
		x1 = x + height;
		y1 = y + width;

		// draw a rectangle with the boundings xy and x1y1, fill it with the color #3399CC
		LCD.drawRect(x, y, x1, y1, FILL, 0x08D);
		// draw rounded Borders
//		LCD.drawImg(x1-5, y, 5 ,5 ,leftBlue);
//		LCD.drawImg(x, y, 5 ,5 ,leftBlue, INVERT);
//		LCD.drawImg(x1-5, y1-4, 5 ,5 ,rightBlue);
//		LCD.drawImg(x, y1-4, 5 ,5 ,rightBlue, INVERT);

		// draw the button text
		LCD.putString(buttonText,x+5,y+5,WHITE, 0x08D);
	}

	else
	{
		// define boundings
		byte x1,y1;
		x1 = x + height;
		y1 = y + width;

		// draw a rectangle with the boundings xy and x1y1, fill it with the color #666666
		LCD.drawRect(x, y, x1, y1, FILL, 0x666);
		// draw rounded Borders
//		LCD.drawImg(x1-5, y, 5 ,5 ,leftGrey);
//		LCD.drawImg(x, y, 5 ,5 ,leftGrey, INVERT);
//		LCD.drawImg(x1-5, y1-4, 5 ,5 ,rightGrey);
//		LCD.drawImg(x, y1-4, 5 ,5 ,rightGrey, INVERT);

		// draw the button text
		LCD.putString(buttonText,x+5,y+5,WHITE,0x666);
	}
}

void SButton::setFocus(byte foc)
{
	curFocus = foc;

	if(lastFocus != curFocus)
			draw();
	lastFocus = curFocus;
}

// void SButton::drawTab(const char* tabText, byte x, byte y, byte width, byte focus)
// {
// 	byte height = 17;
// 	
// 	if(focus == 1)
// 	{
// 	// define boundings
// 	byte x1,y1;
// 	x1 = x + height;
// 	y1 = y + width;
// 
// 	// draw a rectangle with the boundings xy and x1y1, fill it with the color #3399CC
// 	LCD.drawRect(x, y, x1, y1, FILL, 0x3C3);
// 	// draw rounded Borders
// //	LCD.drawImg(x1-5, y, 5 ,5 ,leftGreen);
// //	LCD.drawImg(x1-5, y1-4, 5 ,5 ,rightGreen);
// 
// 	// draw the button text
// 	LCD.putString(tabText,x+5,y+5,WHITE, 0x3C3);
// 	}
// 	else
// 	{
// 	// define boundings
// 	byte x1,y1;
// 	x1 = x + height;
// 	y1 = y + width;
// 
// 	// draw a rectangle with the boundings xy and x1y1, fill it with the color #3399CC
// 	LCD.drawRect(x, y, x1, y1, FILL, 0x666);
// 	// draw rounded Borders
// //	LCD.drawImg(x1-5, y, 5 ,5 ,leftGrey);
// //	LCD.drawImg(x1-5, y1-4, 5 ,5 ,rightGrey);
// 
// 	// draw the button text
// 	LCD.putString(tabText,x+5,y+5,WHITE, 0x666);
// 
// 	}
// }

