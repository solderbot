/*
Copyright 2008  Lukas Kropatschek lukas.krop@gmail.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "SBUI.h"

//#include "StaticStrings.h"

#include <LCD.h>
#include <IOManager.h>
#include <core/defines.h>
#include <SolderManager.h>

#define clearViewArea() LCD.drawRect(21, 0, 109, 130, FILL, WHITE);
#define clearStatusMsgArea() LCD.drawRect(0,0,19,69,FILL,WHITE);


SBUI::SBUI()
{
	total = 0;
}

void SBUI::update()
{
	LCD.putVariable("X:", SolderMgr.curX, 100, 5, RED, WHITE, 30);
	LCD.putVariable("Y:", SolderMgr.curY, 90, 5, BLUE, WHITE, 30);
}

void SBUI::draw()
{
	clearViewArea();

	drawStatusBar();
}

void SBUI::drawCoordInput()
{
	byte xcursor = 0;
	byte ycursor = 0;
	byte curField = 0;
	byte xArray[] = { 0, 0, 0};
	byte yArray[] = { 0, 0, 0};
	byte xpos = 2;
	byte ypos = 2;

	LCD.putChar('X', 65, 5, RED, WHITE);
	LCD.putChar('Y', 50, 5, BLUE, WHITE);

	LCD.drawRect(49, 15, 75, 75, FILL, WHITE);

	LCD.drawRect(64, 15, 75, 75, 0, RED);
	LCD.drawRect(49, 15, 60, 75, 0, GREY);

	while (true)
	{
		// acquire PS2 keycode
		byte ps2 = IOMgr.getPS2Keycode();

		// delete the last character and set cursor position accordingly

		if (ps2 == KEYCODE_BACKSPACE)
		{
			if (curField == 0)
			{
				xArray[xpos] = 0;

				if (xcursor != 0)
					xcursor -= 1;

				if (xpos != 2)
					xpos++;

				// clear with an space char
				LCD.putChar(' ', 65, (16 + (6 * xcursor)), BLACK, WHITE);
			}

			if (curField == 1)
			{
				yArray[ypos] = 0;

				if (ycursor != 0)
					ycursor -= 1;

				if (ypos != 2)
					ypos++;

				LCD.putChar(' ', 50, (16 + (6 * ycursor)), BLACK, WHITE);
			}

		}

		if (ps2 == KEYCODE_TAB)
		{
			//Toogle current coordinate input (x/y)
			if (curField == 0)
			{
				curField = 1;
				LCD.drawRect(64, 15, 75, 75, 0, GREY);
				LCD.drawRect(49, 15, 60, 75, 0, RED);
			}

			else
			{
				curField = 0;
				LCD.drawRect(64, 15, 75, 75, 0, RED);
				LCD.drawRect(49, 15, 60, 75, 0, GREY);
			}
		}

		if (ps2 == KEYCODE_ENTER)
		{
			byte x, y;

			switch (xcursor)
			{

				case 1:
					x = xArray[2];
					break;

				case 2:
					x = xArray[1] + (xArray[2] * 10);
					break;

				case 3:
					x = xArray[0] + (xArray[1] * 10) + (xArray[2] * 100);
					break;

				default:
					x = 0;
					break;
			}

			switch (ycursor)
			{

				case 1:
					y = yArray[2];
					break;

				case 2:
					y = yArray[1] + (yArray[2] * 10);
					break;

				case 3:
					y = yArray[0] + (yArray[1] * 10) + (yArray[2] * 10);
					break;

				default:
					y = 0;
					break;
			}

			//LCD.putVariable("x:", x, 35, 5, BLACK, WHITE, 30);
			//LCD.putVariable("y:", y, 25, 5, BLACK, WHITE, 30);
			SolderMgr.goTo(x, y);

			// get out of the endless loop
			break;
		}

		else
		{
			if (ps2 != 0 && ps2 != KEYCODE_TAB && ps2 != KEYCODE_BACKSPACE && ps2 != KEYCODE_ENTER)
			{
				// do work for the x field
				if (curField == 0)
				{
					if (xcursor <= 2)
					{
						// print char on screen
						LCD.putChar(ps2, 65, (16 + (6 * xcursor)), BLACK, WHITE);
						// add value to data array
						xArray[xpos] = ps2 - 48;
						xcursor++;

						if (xpos != 0)
							xpos--;
					}
				}

				if (curField == 1)
				{
					if (ycursor <= 2)
					{
						LCD.putChar(ps2, 50, (16 + (6 * ycursor)), BLACK, WHITE);
						yArray[ypos] = ps2 - 48;
						ycursor++;

						if (ypos != 0)
							ypos--;
					}
				}
			}
		}
	}

}

void SBUI::drawStatusBar()
{
// draw a line and the current status text
	LCD.drawLine(20, 0, 20, 130, BLACK);
	// draw free SRAM
	LCD.putVariable("SRAM:", availableSRAM(), 5, 70, BLACK, WHITE, 40);

	// check whicht status message should be displayed

	/* switch (code)
	 {

	  case 0:
	   // clear area
	   clearStatusMsgArea();
	   LCD.putString ( (const char*) getPGMStr (Idle), 5, 5, BLACK, WHITE);
	   lastStatusMsg = 0;
	   break;

	  case 1:
	   // only draw status message if it is not the
	   // same as the currently displayed

	   if (lastStatusMsg != 1)
	   {
	    clearStatusMsgArea();
	    LCD.putString ( (const char*) getPGMStr (Solder), 5, 5, BLACK, WHITE);
	   }

	   lastStatusMsg = 1;

	   break;

	  case 2:

	   if (lastStatusMsg != 2)
	   {
	    clearStatusMsgArea();
	    LCD.putString ( (const char*) getPGMStr (Moving), 5, 5, BLACK, WHITE);
	   }

	   lastStatusMsg = 2;

	   break;

	  case 3:

	   if (lastStatusMsg != 3)
	   {
	    clearStatusMsgArea();
	    LCD.putString ( (const char*) getPGMStr (Loading), 5, 5, BLACK, WHITE);
	   }

	   lastStatusMsg = 3;

	   break;
	 }
	 */

}

void SBUI::drawShowTrack()
{
	// draw coord system
	clearViewArea();

	for (byte i = 0; i < 16; i++)
	{
		LCD.drawLine(35, 25 + (5 * i), 105, 25 + (5 * i), LIGHTGREY);

		if (i < 14)
			LCD.drawLine(35 + (5 * i), 25, 35 + (5 * i), 105, LIGHTGREY);
	}

	// y axis
	LCD.drawLine(35, 25, 105, 25, BLACK);

	// x axis
	LCD.drawLine(35, 25, 35, 105, BLACK);

	// loop through all points / 2 doing 2 points each time
	for (byte i = 1; i <= (total / 2); i++)
	{
		// aquire point data
		byte x1;
		byte y1;
		byte x2;
		byte y2;
		x1 = IOMgr.smap[i][1];
		y1 = IOMgr.smap[i][2];
		x2 = IOMgr.smap[i+1][1];
		y2 = IOMgr.smap[i+1][2];

		//coordinate system finished draw points and lines now

		// draw a line from point to point
		LCD.drawLine(35 + y1, 25 + x1, 35 + y2, 25 + x2, LIGHTBLUE);

		// draw points
		LCD.drawX(35 + y1, 25 + x1, RED);
		LCD.drawX(35 + y2, 25 + x2, RED);
	}

	LCD.putVariable("Points:", total, 22, 5, BLACK, WHITE);
}

void SBUI::drawLoadTrack(byte count, byte coord, byte pos)
{
	// if we haven't started to receive
	if (count == 0)
	{
		clearViewArea();
		LCD.putString("Waiting for SMap..", 100, 5, BLACK, WHITE);
	}

	if (count > 0 && count != 101)
	{
		// display what we received yet and the percentage
		LCD.drawRect(100, 5, 110, 100, FILL, WHITE);
		LCD.putVariable("RX:", count, 100, 5, BLACK, WHITE, 30);
		LCD.putVariable("/", total, 100, 42, BLACK, WHITE, 30);
		LCD.putVariable("", ((count * 100) / total), 100, 70, BLACK, WHITE, 3);
		LCD.putChar('%', 100, 92, BLACK, WHITE);

		// display x coord

		if (pos == 1)
		{
			LCD.putVariable("LP(", coord, 90, 5, BLACK, WHITE, 30);
		}

		// display y coord

		if (pos == 2)
		{
			LCD.putVariable("/", coord, 90, 42, BLACK, WHITE, 30);
			LCD.putChar(')', 90, 67, BLACK, WHITE);
		}

	}

	// if we finished receiving the count is set to 101

	if (count == 101)
	{
		LCD.putString("Enter -> start", 70, 5, BLACK, WHITE);
		LCD.putString("V -> show track", 60, 5, BLACK, WHITE);
		while(true)
		{
			byte ps2 = IOMgr.getPS2Keycode();
			
			if(ps2 == KEYCODE_V)
			{
				drawShowTrack();
				break;
			}
			else if(ps2 == KEYCODE_ENTER)
			{
				drawSolder();
				break;
			}
		}
		//TODO Add info message for start soldering
	}
}

void SBUI::drawSolder()
{
	clearViewArea();
	update();
	LCD.putString("Space -> Pause", 70, 5, BLACK, WHITE);
	
	byte i = 0;
	
	while(i <= total)
	{
		byte ps2 = IOMgr.getPS2Keycode();
		
		if(ps2 == KEYCODE_SPACE)
		{
			while(true)
			{
				byte pause = IOMgr.getPS2Keycode();
				if(pause == KEYCODE_SPACE)
					break;
			}
		}
		
		byte x,y;
		
		x = IOMgr.smap[i][1];
		y = IOMgr.smap[i][2];
		
		SolderMgr.goTo(x,y);
		SolderMgr.solder();
		
		i++;
	}

}


SBUI sbui = SBUI();
