/*
Copyright 2008  Lukas Kropatschek lukas.krop@gmail.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SBUI_H
#define SBUI_H

#include <arduino.h>

class SBUI
{
	public:
		SBUI();

		/**
		\brief Draws the main view
		*/
		void draw();
		/**
		\brief Draws the status bar
		\param code defines which message is displayed
		*/
		void drawStatusBar ();
		/**
		\brief Draws the current loaded SMap
		*/
		void drawShowTrack();
		/**
		\brief Draws the view for loading a SMap
		\param count number of current coordinate to receive
		\param coord current coordinate value
		\param pos whether its the x or y coord
		*/
		void drawLoadTrack(byte count, byte coord, byte pos);
		/**
		\brief Updates X and Y coords to get displayed
		*/
		void update();
		
		void drawCoordInput();	
		
		void drawSolder();

		byte total;

	private:
	//	byte lastStatusMsg;
};

extern SBUI sbui;

#endif

