/*
Copyright 2008  Lukas Kropatschek lukas.krop@gmail.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of 
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef STATICSTRINGS_H
#define STATICSTRINGS_H

#include <avr/pgmspace.h>

// status strings
prog_char IdleStr[] PROGMEM = "Idle";
prog_char SolderStr[] PROGMEM = "Soldering";
prog_char MovingStr[] PROGMEM = "Moving";
prog_char LoadingStr[] PROGMEM = "Loading";

// String table
PGM_P stringTable[] PROGMEM =
{
	IdleStr,
	SolderStr,
	MovingStr,
	LoadingStr
};

#define Idle 0
#define Solder 1
#define Moving 2
#define Loading 3

char buffer[8];

char* getPGMStr(byte num)
{
	strcpy_P(buffer, (char*) pgm_read_word(&(stringTable[num])));
	return buffer;
}

#endif

