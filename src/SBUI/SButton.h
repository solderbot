/*
Copyright 2008  Lukas Kropatschek lukas.krop@gmail.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of 
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SBUTTON_H
#define SBUTTON_H

#include <arduino.h>

/**
 * \brief Class for drawing a Button on the Screen
 */
class SButton 
{
	public:
		/**
		 * \brief ctor
		 * \param id button identification
		 * \param text text on the button
		 * \param x left-bottom x-coord
		 * \param y left-bottom y-coord
		 * \param width button width
		 * \param height button height
		 */
		SButton(const char* text, byte x, byte y, byte width, byte height = 17);
		SButton();
		/**
		 * \brief Sets the focus of the button
		 * \param foc wheter the button has focus or not
		 */
		void setFocus(byte foc);
		/**
		 * \brief Draws the button on the screen
		 */
		void draw();
		
// 		void drawTab(const char* tabText, byte x, byte y, byte width, byte focus);

	private:
		byte curFocus;
		byte lastFocus;

		byte x,y,width,height;
		
		const char* buttonText;
};

#endif
