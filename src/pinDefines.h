#ifndef PIN_DEFINES_H
#define PIN_DEFINES_H

#define lcdResPin 7
#define lcdCsPin 6
#define lcdDataPin 5
#define lcdClkPin 4
/*
#define lcdCsPin 3
#define lcdResPin 2
*/

#define ps2ClkPin 3
#define ps2DataPin 2

#endif

