/*
Copyright 2008  Lukas Kropatschek lukas.krop@gmail.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef SERIAL_H
#define SERIAL_H

#define RX_BUFFER_SIZE 128

#include <core/arduino.h>
#include <inttypes.h>

#define DEC 10
#define HEX 16
#define OCT 8
#define BIN 2
#define BYTE 0

/**
 * \brief Class for a serial connection
 */

class HardwareSerial
{

	public:
		HardwareSerial();

		/**
		 * \brief Begins serial communications
		 * \param baud Baudrate
		 */
		void begin (long baud);
		/**
		 * \brief Returns a value greater than zero if something new arrived on the RX
		 * \return a value greater than zero if something new arrived
		 */
		int available();
		/**
		 * \brief Reads data from the RX
		 * \return Data read from the serial RX
		 */
		int read();
		/**
		 * \brief Flushes the data buffer
		 */
		void flush();

		//TODO write better doxygen docs for print and println
		/**
		 * \brief Print something to the TX
		 * With this function you can send data via the serial line
		 */
		void print (char);
		void print (const char[]);
		void print (uint8_t);
		void print (int);
		void print (unsigned int);
		void print (long);
		void print (unsigned long);
		void print (long, int);
		/**
		 * \brief Print something to the TX and make a new line
		 */
		void println (void);
		void println (char);
		void println (const char[]);
		void println (uint8_t);
		void println (int);
		void println (unsigned int);
		void println (long);
		void println (unsigned long);
		void println (long, int);

	private:
		void printByte (byte c);
		void printString (const char *s);
		void printNewline();
		void printIntegerInBase (unsigned long n, unsigned long base);

		void printNumber (unsigned long n, uint8_t base);

		void write (byte);

};

extern HardwareSerial Serial;

#endif

