/*
Copyright 2008  Lukas Kropatschek lukas.krop@gmail.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Serial.h"

#define RX_BUFFER_SIZE 128

unsigned char rxBuffer[RX_BUFFER_SIZE];

int rxBufferHead = 0;
int rxBufferTail = 0;

ISR (USART_RX_vect)
{
	byte c = UDR0;

	int i = (rxBufferHead + 1) % RX_BUFFER_SIZE;

	// if we should be storing the received character into the location
	// just before the tail (meaning that the head would advance to the
	// current location of the tail), we're about to overflow the buffer
	// and so we don't write the character or advance the head.

	if (i != rxBufferTail)
	{
		rxBuffer[rxBufferHead] = c;
		rxBufferHead = i;
	}
}

HardwareSerial::HardwareSerial()
{}

void HardwareSerial::begin (long baud)
{
	// Set Baudrate in the high and low register
	UBRR0H = ( (F_CPU / 16 + baud / 2) / baud - 1) >> 8;
	UBRR0L = ( (F_CPU / 16 + baud / 2) / baud - 1);

	// enable rx and tx
	setBit (UCSR0B, RXEN0);
	setBit (UCSR0B, TXEN0);

	// enable interrupt on complete reception of a byte
	setBit (UCSR0B, RXCIE0);
}

int HardwareSerial::available()
{
	return (RX_BUFFER_SIZE + rxBufferHead - rxBufferTail) % RX_BUFFER_SIZE;
}

int HardwareSerial::read()
{
	// if the head isn't ahead of the tail, we don't have any characters
	if (rxBufferHead == rxBufferTail)
		return -1;

	else
	{
		byte c = rxBuffer[rxBufferTail];
		rxBufferTail = (rxBufferTail + 1) % RX_BUFFER_SIZE;
		return c;
	}
}

void HardwareSerial::flush()
{
	// don't reverse this or there may be problems if the RX interrupt
	// occurs after reading the value of rx_buffer_head but before writing
	// the value to rx_buffer_tail; the previous value of rx_buffer_head
	// may be written to rx_buffer_tail, making it appear as if the buffer
	// were full, not empty.
	rxBufferHead = rxBufferTail;
}

void HardwareSerial::write (byte c)
{
	// wait for a empty buffer
	while (! (UCSR0A & (1 << UDRE0)));

	// write data in the buffer, send data
	UDR0 = c;
}

void HardwareSerial::printByte (byte c)
{
	write (c);
}

void HardwareSerial::printNewline()
{
	printByte ('\n');
}

void HardwareSerial::printString (const char *s)
{
	while (*s)
		printByte (*s++);
}

void HardwareSerial::printIntegerInBase (unsigned long n, unsigned long base)
{
	byte buf[8 * sizeof (long) ]; // Assumes 8-bit chars.
	unsigned long i = 0;

	if (n == 0)
	{
		printByte ('0');
		return;
	}

	while (n > 0)
	{
		buf[i++] = n % base;
		n /= base;
	}

	for (; i > 0; i--)
		printByte (buf[i - 1] < 10 ?
				   '0' + buf[i - 1] :
				   'A' + buf[i - 1] - 10);
}

void HardwareSerial::print (char c)
{
	printByte (c);
}

void HardwareSerial::print (const char c[])
{
	printString (c);
}

void HardwareSerial::print (uint8_t b)
{
	printByte (b);
}

void HardwareSerial::print (int n)
{
	print ( (long) n);
}

void HardwareSerial::print (unsigned int n)
{
	print ( (unsigned long) n);
}

void HardwareSerial::print (long n)
{
	if (n < 0)
	{
		print ('-');
		n = -n;
	}

	printNumber (n, 10);
}

void HardwareSerial::print (unsigned long n)
{
	printNumber (n, 10);
}

void HardwareSerial::print (long n, int base)
{
	if (base == 0)
		print ( (char) n);
	else if (base == 10)
		print (n);
	else
		printNumber (n, base);
}

void HardwareSerial::println (void)
{
	print ('\r');
	print ('\n');
}

void HardwareSerial::println (char c)
{
	print (c);
	println();
}

void HardwareSerial::println (const char c[])
{
	print (c);
	println();
}

void HardwareSerial::println (uint8_t b)
{
	print (b);
	println();
}

void HardwareSerial::println (int n)
{
	print (n);
	println();
}

void HardwareSerial::println (unsigned int n)
{
	print (n);
	println();
}

void HardwareSerial::println (long n)
{
	print (n);
	println();
}

void HardwareSerial::println (unsigned long n)
{
	print (n);
	println();
}

void HardwareSerial::println (long n, int base)
{
	print (n, base);
	println();
}

void HardwareSerial::printNumber (unsigned long n, uint8_t base)
{
	printIntegerInBase (n, base);
}

HardwareSerial Serial = HardwareSerial();

