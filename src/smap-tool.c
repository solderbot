/*
Copyright 2008  Lukas Kropatschek lukas.krop@gmail.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of 
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdio.h>    
#include <stdlib.h> 
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h> 
#include <errno.h> 
#include <termios.h>
#include <sys/ioctl.h>
#include "IOManager/commands.h"

int serialInit(const char* serialport, int baud);
int serialWriteByte(int fd, uint8_t b);
int serialWrite(int fd, const char* str);
int serialReadUntil(int fd, char* buf, char until);

int main(int argc, char *argv[]) 
{
    int serialFD = 0;
    FILE* smapFilePtr;
    char lineBuffer[11];
	char lenBuffer[5];
	int len;
    int n = 1;

	printf("Opening serial connection... 3 secs pause...\n");
	serialFD = serialInit("/dev/ttyUSB1", 9600);
	
	usleep(3000 * 1000);
	
	printf("Opening smap file...\n");
	smapFilePtr = fopen("test.smap","r");
	if(smapFilePtr == NULL)
		perror("Error opening 'test.smap' file");
	
	printf("Parsing and transmitting...\n");

	serialWriteByte(serialFD, BEGIN_SMAP);
	
	len = atoi(fgets(lenBuffer,5,smapFilePtr));
	
	serialWriteByte(serialFD, SMAP_TOTAL);
	serialWriteByte(serialFD, len);

	while(n <= len)
	{
		char* coord = 0;
		int x;
		int y;
		
		fgets(lineBuffer, 11, smapFilePtr);

		coord = strtok(lineBuffer, "(,) \n");
		x = atoi(coord);
		coord = strtok(NULL, "(,) \n");
		y = atoi(coord);

		printf("[%0*hd/%0*hd] = (%d,%d)\n",2,n,2,len,x,y);

		serialWriteByte(serialFD, SMAP_COUNT);
		serialWriteByte(serialFD, n);
		serialWriteByte(serialFD, SMAP_X);
		serialWriteByte(serialFD, x);
		serialWriteByte(serialFD, SMAP_Y);
		serialWriteByte(serialFD, y);
	
		usleep(500 * 1000);
		n++;
	}

	serialWriteByte(serialFD, END_SMAP);

	while(1){}

    exit(EXIT_SUCCESS);    
} 
    
int serialWriteByte( int fd, uint8_t b)
{
    int n = write(fd,&b,1);
    if( n!=1)
        return -1;
    return 0;
}

int serialWrite(int fd, const char* str)
{
    int len = strlen(str);
    int n = write(fd, str, len);
    if( n!=len ) 
        return -1;
    return 0;
}

int serialReadUntil(int fd, char* buf, char until)
{
    char b[1];
    int i=0;
    do { 
        int n = read(fd, b, 1);  // read a char at a time
        if( n==-1) return -1;    // couldn't read
        if( n==0 ) {
            usleep( 10 * 1000 ); // wait 10 msec try again
            continue;
        }
        buf[i] = b[0]; i++;
    } while( b[0] != until );

    buf[i] = 0;  // null terminate the string
    return 0;
}

int serialInit(const char* serialport, int baud)
{
    struct termios toptions;
    int fd;
    
    fd = open(serialport, O_RDWR | O_NOCTTY | O_NDELAY);
    if (fd == -1)  {
        perror("serialInit: Unable to open port ");
        return -1;
    }
    
    if (tcgetattr(fd, &toptions) < 0) {
        perror("serialInit: Couldn't get term attributes");
        return -1;
    }
    speed_t brate = baud; // let you override switch below if needed
    switch(baud) {
    case 4800:   brate=B4800;   break;
    case 9600:   brate=B9600;   break;
    case 19200:  brate=B19200;  break;
    case 38400:  brate=B38400;  break;
    case 57600:  brate=B57600;  break;
    case 115200: brate=B115200; break;
    }
    cfsetispeed(&toptions, brate);
    cfsetospeed(&toptions, brate);

    // 8N1
    toptions.c_cflag &= ~PARENB;
    toptions.c_cflag &= ~CSTOPB;
    toptions.c_cflag &= ~CSIZE;
    toptions.c_cflag |= CS8;
    // no flow control
    toptions.c_cflag &= ~CRTSCTS;

    toptions.c_cflag |= CREAD | CLOCAL;  // turn on READ & ignore ctrl lines
    toptions.c_iflag &= ~(IXON | IXOFF | IXANY); // turn off s/w flow ctrl

    toptions.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG); // make raw
    toptions.c_oflag &= ~OPOST; // make raw

    // see: http://unixwiz.net/techtips/termios-vmin-vtime.html
    toptions.c_cc[VMIN]  = 0;
    toptions.c_cc[VTIME] = 20;
    
    if( tcsetattr(fd, TCSANOW, &toptions) < 0) {
        perror("serialInit: Couldn't set term attributes");
        return -1;
    }

    return fd;
}
