/*
Copyright 2008  Lukas Kropatschek lukas.krop@gmail.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <arduino.h>

#include <SBUI.h>
#include <Serial.h>
#include <IOManager.h>
#include <LCD.h>
#include <PS2.h>

#include <Debug.h>
#include <SolderManager.h>

int main()
{
	init();
	setup();

	for (;;)
		loop();


}

void setup()
{
	// begin serial communications with a 9600 Baudrate
	Serial.begin(9600);

	// init the LCD screen driver
	LCD.init();

	// init ps2 device
	kbd.begin();

	// draw the main menu
	sbui.draw();
}

void loop()
{
	IOMgr.checkInput();
#ifdef _DEBUG_
	/* debug.debugConsole();
	 while(true)
	 {
	  SolderMgr.manualStep(0,500);
	  delay(500);
	  SolderMgr.manualStep(0,-500);
	  delay(500);

	  SolderMgr.manualStep(1,500);
	  delay(500);
	  SolderMgr.manualStep(1,-500);
	  delay(500);

	  SolderMgr.manualStep(2,500);
	  delay(500);
	  SolderMgr.manualStep(2,-500);
	  delay(500);
	 }
	*/
#endif
}

