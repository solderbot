/*
Copyright 2008  Lukas Kropatschek lukas.krop@gmail.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifdef _DEBUG_

#include "Debug.h"
#include <Serial.h>
#include <SolderManager.h>


Debug::Debug()
{
}

void Debug::write (const char* str)
{
	Serial.println (str);
}

void Debug::debugConsole()
{
	qflag = 0;

	Serial.print ("> ");

	while (qflag == 0)
	{

		if (Serial.available() > 0)
		{
			byte in;
			in = Serial.read();

//    Serial.println(in);

			switch (in)
			{
//     case 'w':
//      break;
//
				case 'a':
					SolderMgr.manualStep(2, 20);
				break;
//
				case 's':
					SolderMgr.manualStep(2, -20);
				break;
//
//     case 'd':
//      break;

				case '1':
					// increase speed by 10 of X stepper
					SolderMgr.setSpeed (2, SolderMgr.getSpeed (2) + 10);
					// print it out
					Serial.print ("X:speed: ");
					Serial.println (SolderMgr.getSpeed (2));
					break;

				case '2':
					// decrease speed by 10 of X stepper
					SolderMgr.setSpeed (2, SolderMgr.getSpeed (2) - 10);
					// print it out
					Serial.print ("X:speed: ");
					Serial.println (SolderMgr.getSpeed (2));
					break;

				case '3':
					SolderMgr.setSpeed (1, SolderMgr.getSpeed (1) + 10);
					Serial.print ("Y:speed: ");
					Serial.println (SolderMgr.getSpeed (1));
					break;

				case '4':
					SolderMgr.setSpeed (1, SolderMgr.getSpeed (1) - 10);
					Serial.print ("Y:speed: ");
					Serial.println (SolderMgr.getSpeed (1));
					break;

				case '5':
					// increase max steps of X stepper by 10
					SolderMgr.setMaxSteps (2, SolderMgr.getMaxSteps (2) + 10);
					Serial.print ("X:max: ");
					Serial.println (SolderMgr.getMaxSteps (2));
					break;

				case '6':
					// decrease max steps of X stepper by 10
					SolderMgr.setMaxSteps (2, SolderMgr.getMaxSteps (2) - 10);
					Serial.print ("X:max: ");
					Serial.println (SolderMgr.getMaxSteps (2));
					break;

				case '7':
					SolderMgr.setMaxSteps (1, SolderMgr.getMaxSteps (1) + 10);
					Serial.print ("Y:max: ");
					Serial.println (SolderMgr.getMaxSteps (1));
					break;

				case '8':
					SolderMgr.setMaxSteps (1, SolderMgr.getMaxSteps (1) - 10);
					Serial.print ("Y:max: ");
					Serial.println (SolderMgr.getMaxSteps (1));
					break;

				case 'h':
					SolderMgr.manualStep (0, 20);
					break;

				case 'j':
					SolderMgr.manualStep (1, 1);
					break;

				case 'k':
					SolderMgr.manualStep (1, -1);
					break;

				case 'l':
					SolderMgr.manualStep (0, -20);
					break;

				default:
					Serial.println ("-");
					break;
			}
		}
	}
}

Debug debug = Debug();

#endif
