/*
Copyright 2008  Lukas Kropatschek lukas.krop@gmail.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

S1D15G10 LCD Driver

 CONNECT GRAPH:

 Arduino ------ Nokia LCD Board
         2 -- 2
         3 -- 5
         4 -- 4
         5 -- 3

Display commands */

#ifndef LCD_H
#define LCD_H

#define DISON       0xaf
#define DISOFF      0xae
#define DISNOR      0xa6
#define DISINV      0xa7
#define COMSCN      0xbb
#define DISCTL      0xca
#define SLPIN       0x95
#define SLPOUT      0x94
#define PASET       0x75
#define CASET       0x15
#define DATCTL      0xbc
#define RGBSET8     0xce
#define RAMWR       0x5c
#define RAMRD       0x5d
#define PTLIN       0xa8
#define PTLOUT      0xa9
#define RMWIN       0xe0
#define RMWOUT      0xee
#define ASCSET      0xaa
#define SCSTART     0xab
#define OSCON       0xd1
#define OSCOFF      0xd2
#define PWRCTR      0x20
#define VOLCTR      0x81
#define VOLUP       0xd6
#define VOLDOWN     0xd7
#define TMPGRD      0x82
#define EPCTIN      0xcd
#define EPCOUT      0xcc
#define EPMWR       0xfc
#define EPMRD       0xfd
#define EPSRRD1     0x7c
#define EPSRRD2     0x7d
#define NOP         0x25

// Color codes
#define WHITE 0xFFF
#define BLACK 0x000
#define RED  0xF00
#define GREEN 0x0F0
#define BLUE 0x00F
#define CYAN 0x0FF
#define MAGENTA 0xF0F
#define YELLOW 0xFF0
#define BROWN 0xB22
#define ORANGE 0xFA0
#define PINK 0xF6A
#define GREY 0x777
#define LIGHTGREY 0xDDD
#define LIGHTRED 0xFAA
#define LIGHTBLUE 0xAAF

#define FILL 1
#define INVERT 1

#include <avr/pgmspace.h>

/**
 * \brief Epson S1D15G10 display driver for the Arduino
 */

class LCDDriver
{

	public:
		/**
		 * \brief Initialises the display
		 */
		void init();
		/**
		 * \brief Initiatlises the Screen and sets the pins
		 * \param resPin Reset pin
		 * \param csPin Chipselect pin
		 * \param clockPin Clock pin
		 * \param dataPin Data pin
		 */
		LCDDriver (byte resPin, byte csPin, byte clockPin, byte dataPin);

		/**
		 * \brief Clears screen with specified color
		 * \param color color to use
		 */
		void clearScreen (int color);
		/**
		 * \brief Draw a rectangel on the screen
		 * \param x0 Left bottom x-coord
		 * \param y0 Left bottom y-coord
		 * \param x1 Right top x-cord
		 * \param y1 Right top y-cord
		 * \param fill whether to fill the rectangle
		 * \param color color of the rectangel
		 */
		void drawRect (byte x0, byte y0, byte x1, byte y1, byte fill, int color);
		/**
		 * \brief Draw a circle on the screen
		 * \param x0 x-coord of the center of the circle
		 * \param y0 y-coord of the center of the circle
		 * \param radius radius of the circle
		 * \param color color of the circle
		 */
//   void drawCircle(byte x0, byte y0, byte radius, int color);
		/**
		 * \brief Draw a line on the screen
		 * \param x0 line start x-coord
		 * \param y0 line start y-coord
		 * \param x1 line end x-coord
		 * \param y1 line end y-coord
		 * \param color color of the line
		 */
		void drawLine (byte x0, byte y0, byte x1, byte y1, int color);
		/**
		 * \brief Draw a single pixel on the screen
		 * \param x x-coord of the pixel
		 * \param y y-coord of the pixel
		 * \param color color of the pixel
		 * This driver uses the 12bit per pixel Type-B mode
		 * as discribed in the Epson S1D15G10 Datasheet.
		 * So the color is held in a 16bit int variable.
		 * The first 4 bits are dummybits and 2 byte of
		 * data get shifted out like this:
		 * 1. write:
		 *  XXXX RRRR
		 * 2. write:
		 *  GGGG BBBB
		 */
		void drawPixel (byte x, byte y, int color);
		/**
		 * \brief Increases the contrast of the display
		 * \param strength how much the contrast should be increased
		 */
//   void increaseContrast(byte strength);
		/**
		 * \brief Decreases the contrast of the display
		 * \param strength how much the contrast should be decreased
		 */
//   void decreaseContrast(byte strenght);
		/**
		 * \brief Draws a single ASCII character
		 * \param c ASCII character to draw
		 * \param x Bottom left x-coord
		 * \param y Bottom left y-coord
		 * \param fColor Foregroundcolor
		 * \param bColor Backgroundcolor
		 */
		void putChar (char c, byte x, byte y, int fColor, int bColor);
		/**
		 * \brief Draws a ASCII String on the screen
		 * \param string The ASCII string
		 * \param x Bottom left starting x-coord
		 * \param y Bottom left starting y-coord
		 * \param fColor Foregroundcolor
		 * \param bColor Backgroundcolor
		 */
		void putString (const char* string, byte x, byte y, int fColor, int bColor);
		/**
		 * \brief Draws a Variable and its name on the screen
		 * \param varname name of the variable
		 * \param value value of the variable
		 * \param x Bottom left starting x-coord
		 * \param y Bottom left starting y-coord
		 * \param fColor Foregroundcolor
		 * \param bColor Backgroundcolor
		 * \param dig Digets: 2 = _x, 20 = 0x, 3 = __x, 30 = 00x 
		 */
		void putVariable (const char* varname, int value, byte x, byte y, int fColor, int bColor, byte dig = 0);
		/**
		 * \brief Draw a Image from color array
		 * \param x Bottom left x-coord
		 * \param y Bottom left y-coord
		 * \param width Image width in pixels
		 * \param height Image height in pixels
		 * \param img Array containing color information
		 * \param invert inverts the image if 1, default is 0 = no invert
		 * Please note that the array must start with the first pixel color information
		 * where the first pixel ist the first top left. The second has to be
		 * the one right from the first and so on.
		 * The array has to be in FLASH (use PROGMEM).
		 */
//   void drawImg(byte x, byte y, byte width, byte height, prog_uint16_t img[], byte invert = 0);

		/**
		 * \brief Draws a 3x3 pixel big crosshair
		 * \param x center x - coord
		 * \param y center y - coord
		 * \param color color of the crosshair
		 */
		void drawX (byte x, byte y, int color);

	private:
		/**
		 * \brief Shift out a display command via SPI
		 * \param cmd the command
		 * The command is shifted out from MSB to LSB
		 */
		void spiCmd (byte cmd);
		/**
		 * \brief Shift out data via SPI
		 * \param data the data
		 * The data is shifted out from MSB to LSB
		 */
		void spiData (byte data);

		byte resetPin;
		byte csPin;
		byte clockPin;
		byte dataPin;


};

extern LCDDriver LCD;

#endif

