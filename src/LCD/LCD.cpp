/*
Copyright 2008  Lukas Kropatschek lukas.krop@gmail.com

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <core/arduino.h>
#include "LCD.h"
#include "fonts.h"
#include <pinDefines.h>

/*
// set CS LOW, set CLK HIGH
#define SPIENABLE()  { (PORTD)=(((PORTD) & B00100100) | (B00010000)); }

// set CS HIGH, set CLK HIGH
#define SPIDISABLE() { (PORTD)=(((PORTD) & B00100100) | (B00011000)); }

// shift bit out
#define SPIOUT(bit) { PORTD = (B00000100 | (((bit) > 0) << 5)); \
       PORTD = (B00000100 | (((bit) > 0) << 5) | 0x10); }
*/

// set CS LOW, set CLK HIGH
#define SPIENABLE()  { (PORTD)=(((PORTD) & B10100000) | (B00010000)); }

// set CS HIGH, set CLK HIGH
#define SPIDISABLE() { (PORTD)=(((PORTD) & B10100000) | (B10010000)); }

// shift bit out
#define SPIOUT(bit) { PORTD = (B10000000 | (((bit) > 0) << 5)); \
		PORTD = (B10000000 | (((bit) > 0) << 5) | 0x10); }



LCDDriver::LCDDriver (byte resPin, byte chipselectPin, byte clkPin, byte datPin)
{
	resetPin = resPin;
	csPin = chipselectPin;
	clockPin = clkPin;
	dataPin = datPin;

	pinMode (resetPin, OUTPUT);
	pinMode (csPin, OUTPUT);
	pinMode (clockPin, OUTPUT);
	pinMode (dataPin, OUTPUT);
}


void LCDDriver::init()
{
	setPin (csPin, HIGH);
	setPin (resetPin, LOW);
	delay (100);
	setPin (resetPin, HIGH);

	spiCmd (DISCTL);
	spiData (0x00);
	spiData (0x20);
	spiData (0x00);

	spiCmd (COMSCN);
	spiData (1);

	spiCmd (OSCON);

	spiCmd (SLPOUT);

	spiCmd (PWRCTR);
	spiData (0x0F);

	spiCmd (TMPGRD);
	spiData (2);

	spiCmd (DISINV);

	spiCmd (VOLCTR);
	spiData (25); // 15 41
	spiData (4); // 6 3

	spiCmd (DATCTL);
	spiData (0x01);
	spiData (0x00);
	spiData (0x04);

	delay (500);

	clearScreen (0xFFF);

	spiCmd (DISON);
}

void LCDDriver::clearScreen (int color)
{
	long i;
	spiCmd (PASET);
	spiData (0);
	spiData (131);

	spiCmd (CASET);
	spiData (0);
	spiData (131);

	spiCmd (RAMWR);

	for (i = 0;i < ( (131*131));i++) // /2
	{
		spiData (color >> 8);
		spiData (color);
	}


}

void LCDDriver::spiCmd (byte cmd)
{
	SPIENABLE();

	SPIOUT (LOW);

	// send data from MSB to LSB
	SPIOUT (cmd & 0x80);
	SPIOUT (cmd & 0x40);
	SPIOUT (cmd & 0x20);
	SPIOUT (cmd & 0x10);
	SPIOUT (cmd & 0x8);
	SPIOUT (cmd & 0x4);
	SPIOUT (cmd & 0x2);
	SPIOUT (cmd & 0x1);

	SPIDISABLE();
}

void LCDDriver::spiData (byte data)
{
	SPIENABLE();

	SPIOUT (HIGH);

	// send data from MSB to LSB
	SPIOUT (data & 0x80);
	SPIOUT (data & 0x40);
	SPIOUT (data & 0x20);
	SPIOUT (data & 0x10);
	SPIOUT (data & 0x8);
	SPIOUT (data & 0x4);
	SPIOUT (data & 0x2);
	SPIOUT (data & 0x1);

	SPIDISABLE();
}

/* void LCDDriver::drawImg(byte x, byte y, byte width, byte height,prog_uint16_t img[], byte invert)
 {
  if(invert == 1)
  {
   int n;
   byte i,j;
   unsigned int word;

   n = 0;
   while (n < (width*height))
   {
   for(i = x; i < (height+x); i++)
   {
    for(j = y; j < (width+y); j++)
    {
     word = pgm_read_word_near(&img[n]);
     //word = word >> 4;
     drawPixel(i,j,word);
     n++;
    }
   }
   }

  }
  else
  {
   int n;
   byte i,j;
   unsigned int word;

   n = 0;
   while (n < (width*height))
   {
   for(i = (height+x); i > x; i--)
   {
    for(j = y; j < (width+y); j++)
    {
     word = pgm_read_word_near(&img[n]);
     //word = word >> 4;
     drawPixel(i,j,word);
     n++;
    }
   }
   }
  }
 }*/

void LCDDriver::drawPixel (byte x, byte y, int color)
{

	spiCmd (PASET);
	spiData (x);
	spiData (x);

	spiCmd (CASET);
	spiData (y);
	spiData (y);

	spiCmd (RAMWR);
	spiData (color >> 8);
	spiData (color);

	spiCmd (NOP);
}

void LCDDriver::drawRect (byte x0, byte y0, byte x1, byte y1, byte fill, int color)
{

	int xmin, xmax, ymin, ymax;
	int i;

	if (fill == FILL)
	{
		xmin = (x0 <= x1) ? x0 : x1;
		xmax = (x0 > x1) ? x0 : x1;
		ymin = (y0 <= y1) ? y0 : y1;
		ymax = (y0 > y1) ? y0 : y1;

		spiCmd (PASET);
		spiData (xmin);
		spiData (xmax);

		spiCmd (CASET);
		spiData (ymin);
		spiData (ymax);

		spiCmd (RAMWR);

		for (i = 0; i < ( ( ( (xmax - xmin + 1) * (ymax - ymin + 1))) + 130); i++)
		{
			spiData (color >> 8);
			spiData (color);
		}

		spiCmd (NOP);

	}

	else
	{
		drawLine (x0 , y0, x1, y0, color);
		drawLine (x0 , y1, x1, y1, color);
		drawLine (x0, y0, x0, y1, color);
		drawLine (x1, y0, x1, y1, color);
	}
}

void LCDDriver::drawLine (byte x0, byte y0, byte x1, byte y1, int color)
{
	int dy = y1 - y0;
	int dx = x1 - x0;
	int stepx, stepy;

	if (dy < 0)
	{
		dy = -dy;
		stepy = -1;
	}

	else
	{
		stepy = 1;
	}

	if (dx < 0)
	{
		dx = -dx;
		stepx = -1;
	}

	else
	{
		stepx = 1;
	}

	dy <<= 1;   // dy is now 2*dy

	dx <<= 1;   // dx is now 2*dx

	drawPixel (x0, y0, color);

	if (dx > dy)
	{
		int fraction = dy - (dx >> 1); // same as 2*dy - dx

		while (x0 != x1)
		{
			if (fraction >= 0)
			{
				y0 += stepy;
				fraction -= dx; // same as fraction -= 2*dx
			}

			x0 += stepx;

			fraction += dy;  // same as fraction -= 2*dy
			drawPixel (x0, y0, color);
		}
	}

	else
	{
		int fraction = dx - (dy >> 1);

		while (y0 != y1)
		{
			if (fraction >= 0)
			{
				x0 += stepx;
				fraction -= dy;
			}

			y0 += stepy;

			fraction += dx;
			drawPixel (x0, y0, color);
		}
	}
}

/* void LCDDriver::drawCircle(byte x0, byte y0, byte radius, int color)
 {
  int f = 1 - radius;
  int ddF_x = 0;
  int ddF_y = -2 * radius;
  int x = 0;
  int y = radius;

  drawPixel(x0, y0 + radius, color);
  drawPixel(x0, y0 - radius, color);
  drawPixel(x0 + radius, y0, color);
  drawPixel(x0 - radius, y0, color);

  while(x < y)
  {
      if(f >= 0)
   {
         y--;
        ddF_y += 2;
        f += ddF_y;
      }
      x++;
      ddF_x += 2;
      f += ddF_x + 1;
      drawPixel(x0 + x, y0 + y, color);
      drawPixel(x0 - x, y0 + y, color);
      drawPixel(x0 + x, y0 - y, color);
      drawPixel(x0 - x, y0 - y, color);
      drawPixel(x0 + y, y0 + x, color);
      drawPixel(x0 - y, y0 + x, color);
      drawPixel(x0 + y, y0 - x, color);
      drawPixel(x0 - y, y0 - x, color);
   }
 }

 void LCDDriver::increaseContrast(byte strength)
 {
  for(unsigned int i = 0; i <= strength; i++)
  {
   spiCmd(VOLUP);
   delay(20);
  }

 }

 void LCDDriver::decreaseContrast(byte strength)
 {
  for(unsigned int i = 0; i <= strength; i++)
  {
   spiCmd(VOLDOWN);
   delay(20);
  }

 }
*/
void LCDDriver::putChar (char c, byte x, byte y, int fColor, int bColor)
{
#define cols 6
#define rows 8

	int i, j;
	byte Char, charData;
	unsigned int color0, color1;
	unsigned int mask;

	Char = (c - 31);

	spiCmd (PASET);
	spiData (x);
	spiData (x + (rows - 1));
	spiCmd (CASET);
	spiData (y);
	spiData (y + (cols - 1));
	spiCmd (RAMWR);

	for (i = (rows - 1); i >= 0; i--)
	{
		charData = pgm_read_byte (&FONT6x8[Char][i]);
		mask = 0x80;

		for (j = 0; j < cols; j += 2)
		{
			if ( (charData & mask) == 0)
				color0 = bColor;
			else
				color0 = fColor;

			mask = mask >> 1;

			if ( (charData & mask) == 0)
				color1 = bColor;
			else
				color1 = fColor;

			mask = mask >> 1;

			spiData (color0 >> 8);

			spiData (color0);

			spiData (color1 >> 8);

			spiData (color1);
		}
	}
}



void LCDDriver::putString (const char *pString, byte x, byte y, int fColor, int bColor)
{
	while (*pString != 0x00)
	{
		putChar (*pString++, x, y, fColor, bColor);
		y = y + 6;

		if (y > 131) break;
	}
}

void LCDDriver::putVariable (const char *varname, int value, byte x, byte y, int fColor, int bColor, byte dig)
{

	char buffer[20];

	switch (dig)
	{

		case 2:
			sprintf (buffer, "%s%2d", varname, value);
			break;

		case 20:
			sprintf (buffer, "%s%02d", varname, value);
			break;

		case 3:
			sprintf (buffer, "%s%3d", varname, value);
			break;

		case 30:
			sprintf (buffer, "%s%03d", varname, value);
			break;

		case 4:
			sprintf (buffer, "%s%4d", varname, value);
			break;

		case 40:
			sprintf (buffer, "%s%04d", varname, value);
			break;

		default:
			sprintf (buffer, "%s%d", varname, value);
			break;
	}

	const char* result = buffer;

	while (*result != 0x00)
	{
		putChar (*result++, x, y, fColor, bColor);
		y = y + 6;

		if (y > 131) break;
	}
}

void LCDDriver::drawX (byte x, byte y, int color)
{
	drawPixel (x - 1, y + 1, color);
	drawPixel (x - 1, y - 1, color);

	drawPixel (x, y, color);

	drawPixel (x + 1, y + 1, color);
	drawPixel (x + 1, y - 1, color);
}

LCDDriver LCD = LCDDriver (lcdResPin, lcdCsPin, lcdClkPin, lcdDataPin);

