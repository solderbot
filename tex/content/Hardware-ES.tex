%
% Copyright 2008  Lukas Kropatschek lukas.krop@gmail.com
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License as
% published by the Free Software Foundation; either version 2 of
% the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
\chapter{Hardware eingebetteter Systeme}
Jedes eingebettete System benötigt mindestens einen Prozessor, dieser kann in Form eines Mikroprozessors oder eines
Mikrocontrollers vorhanden sein. Ein Mikrocontroller beinhaltet bereits einige Komponenten im Gehäuse (siehe \ref{aufbau}), welche
ein Mikroprozessor erst extern zugeschaltet bekommen müsste.\\
Da sich die Anzahl an Bauteilen auf die Kosten eines Gerätes auswirkt, gibt es den ständigen Trend die Anzahl der
Bauteilen zu minimieren. Das Ziel ist hohe Funktionalität mit wenigen Chips zu erreichen.Trotzdem
sollen Mikrorechner universell sein, um möglichst hohe Stückzahlen produzieren zu können.
\begin{quote}
\textit{``Der Mikroprozessor oder auch CPU ist in seiner klassischen Form ein eigener Chip, der für den
Betrieb noch mit Speichern, I/O-Bausteinen und weiteren Komponenten ergänzt werden muss. Deshalb
führte die Entwicklung zu Mikrocontrollern, die ein funktionsfähiges System auf einem Chip enthalten können''}\\
\cite[Bollow/Homann/Köhn, 2009, S. 14]{Koehn2009}
\end{quote}
So kann man in Tabelle \ref{mc-mp-tbl} festlegen:\\
\begin{center}
\begin{tabular}[!h]{lll}
\hline
\rowcolor{gray80}
\textbf{Eigenschaft} & \textbf{Mikroprozessor} & \textbf{Mikrocontroller}\\
\rowcolor{white}
Hardware-Flexibilität & sehr hoch & gering \\
Varianten-Vielfalt & gering & hoch\\
\hline
\end{tabular}
 \captionof{table}{Mikroprozessor/Mikrocontroller \cite[Bollow/Homann/Köhn, 2009, S. 14]{Koehn2009}}
\label{mc-mp-tbl}
\end{center}

\section{Aufbau eines Mikrocontrollers}\label{aufbau}
Ein Mikrocontroller besteht aus mehreren Komponenten, welche alle im Gehäuse des Controllers verbaut sind. Die
meisten Mikrocontroller sind ähnlich aufgebaut. In Abbildung \ref{mc-aufbau} ist eine vereinfachte
Darstellung des Aufbaus eines Mikrocontrollers zu sehen.
\begin{figure}[h]
\begin{center}
\includegraphics[bb=0 0 640 400, scale=0.6]{img/mc-aufbau.jpg}
\end{center}
\caption{Aufbau eines Mikrocontrollers \cite[vereinfachte Darstellung: Schmitt, 2008, S. 33]{Schmitt2008}}
\label{mc-aufbau}
\end{figure} 
\\
\\Folgende Auflistung beschreibt die Komponente eines Mikrocontrollers im Detail:\\
\cite[vgl. Mittermayr, 2008, S. 53]{Mittermayr2008}
\begin{itemize}
\item \textbf{Prozessor:} Der Prozessor selbst ist noch in mehrere Komponenten unterteilt. Dort befinden sich zum Beispiel die 
ALU\footnote{Arithmetic Logic Unit - Arithmetiklogikeinheit}, die Arbeits- und Statusregister, der Program Counter, der Stack Pointer,
und einige weitere Komponenten.
\item \textbf{SRAM:} steht für \textbf{S}tatic \textbf{R}andom \textbf{A}ccess \textbf{M}emory. Dies ist der
Arbeitsspeicher des Mikrocontrollers. Sein Inhalt wechselt während dem Betrieb meist sehr schnell und geht mit
dem Ausschalten der Versorgungsspannung unwiderruflich verloren.
\item \textbf{EEPROM/Flash:} Dies ist der sogenannte Festwertspeicher. Er könnte mit einer Festplatte im herkömmlichen Computer
verglichen werden, wenngleich weder der EEPROM- (Electrical Eraseable Programmable Read Only Memory) noch der Flash-Speicher so große
Mengen an Daten speichern können. Der große Vorteil dieser Speichereinheiten ist, dass ihr Inhalt nach dem abschalten der Versorgungsspannung
bestehen bleibt und später wieder ausgelesen werden kann.
\item \textbf{Interrupt-Controller:} Diese Einheit kümmert sich um die gezielte Unterbrechung des laufenden Programms. Wenn
sich gewisse Zustände ändern und der Programmierer einen Interrupt (Unterbrechung) zuvor im Quelltext festgelegt hat so
sorgt dieser Controller dafür, dass das Programm unterbrochen, der Interrupt-Code ausgeführt und das Programm wieder an der
richtigen Stelle fortgesetzt wird.
\item \textbf{Timer/Counter:} Diese Komponente ist ein einfacher Zähler, dessen \\
Zählgeschwindigkeit und Bereich eingestellt werden kann.
Zum Beispiel kann bei entsprechender Konfiguration ein Überlauf eines Zählers zu einem Interrupt führen.
\item \textbf{Digital I/O:} Die digitalen Ein-/Ausgangsleitungen sind sozusagen die Schnittstelle zur Umwelt. Diese Leitungen sind
in Ports unterteilt wobei ein Port aus 8 Pins (in manchen fällen auch weniger) besteht. Sie können verwendet werden um Zustände
zu messen oder Werte auszugeben. Es wird allerdings nur zwischen 0 und 1 (\textit{HIGH} und \textit{LOW}) unterschieden. Welche
Spannung \textit{LOW} und welche \textit{HIGH} ist, variiert zwischen den einzelnen Mikrocontrollern.
\item \textbf{Analog I/O:} Viele Mikrocontroller besitzen neben den digitalen auch analoge Ein-/Ausgangsleitungen. Diese sind
in der Lage mehr oder weniger genau Spannungen zu messen. Die analogen Leitungen werden auch Kanäle genannt. Sie geben im
Gegensatz zu den digitalen Leitungen Messwerte wie ``2,4 V'' aus anstatt einer simplen Unterscheidung zwischen 0 und 1.
\end{itemize}


\section{AVR-RISC-Familie}
\begin{wrapfigure}{r}{200px}
\begin{center}
\includegraphics[bb=0 0 150 118, scale=1]{img/atmega-cent.jpg}
\end{center}
\caption{ATMega8 im kleinen TQFP-Gehäuse.}
\label{atmega-cent}
\end{wrapfigure}
In diesem Projekt wurde auf einen Mikrocontroller aus der Atmel\textregistered~ AVR-RISC-Familie
gesetzt da die Atmel Prozessoren sich im Bereich der Hobbyprojekte gut etabliert haben. Dies liegt daran, dass sie
sowohl kostengünstig als auch leicht erhältlich sind und eine große Anhängerschaft besitzen. Es handelt sich bei den AVR-Controllern 
um eine 8 Bit\footnote{8 Bit breite Register und Befehle} Mikrocontroller-Familie die das \textbf{R}educed
\textbf{I}nstruction \textbf{S}et \textbf{C}omputing (RISC) verwendet.
\begin{quote}
\textit{``\textbf{RISC-Prozessoren} haben einfache Befehlssätze mit wenig Mikrocode, großteils Einzyklusmaschinenbefehle,
feste Befehlslängen, einfache Addressierungsverfahren und eine ausgeprägte Pipeline-Architektur.''}\\
\cite[Hansen/Neumann, 2005, S. 77]{wirtschaftsinformatik2}
\end{quote}
Wobei im herkömmlichen PC, und manchen speziellen Mikroprozessoren, auf das \textbf{C}omplex
\textbf{I}nstruction \textbf{S}et \textbf{C}omputing gesetzt wird (CISC), verzichten
die AVR-RISC-Prozessoren zugunsten von Geschwindigkeit auf komplexe Befehle. Dies
hat allerdings den geringen Nachteil, das CISC-Befehle durch mehrere RISC-Befehle
nachgebildet werden müssen, die benötigt dann üblicherweise mehrere Rechenzyklen,
wobei ein einzelner RISC-Befehl i.d.R. nur einen Rechenzyklus der CPU 
(\textbf{C}entral \textbf{P}rocessing \textbf{U}nit) benötigt.\\
\\
Die Firma Atmel behauptet \textit{``AVR''} hätte keine
Bedeutung, es wird aber gemunkelt, dass es von den beiden Entwicklern, Alf Egin Bogen und
Vegard Wollan (\textit{''Alf Vegard RISC"}) hergeleitet sein könnte. RISC, zu Deutsch \textit{Rechnen mit reduzierten Befehlssatz},
hat den Vorteil, dass die Recheneinheit einen geringer Dekodierungsaufwand beim Verarbeiten der einzelnen Befehle benötigt und
deswegen schneller arbeitet. So ist es auch möglich
mit geringer Verzögerung auf sogenannte Interrupts (Programmunterbrechungen) zu reagieren.
Diese Verzögerungszeit kann bei einem CISC-Prozessor stark variieren, wenn gerade ein 
Mikroprogrammabschnitt ausgeführt wird.\\
\\
Der \texttt{ATMega168} besitzt 131 Befehlssätze, welche fast alle nur einen Rechenzyklus
benötigen, außerdem sind diese Befehle physikalisch im Mikrocontroller verbaut, anders
als bei einem CISC-Prozessor, wo die Befehlssätze auf einem eigenen Chip gespeichert sind.
Desweiteren hat der \texttt{ATMega168} 32 Stück, 8-Bit breite, allgemeine Register, über welche
man beispielsweise den Status von I/O-Pins (Ein-/Ausgang-Leitungen) überprüfen oder setzten
kann.
% \subsection*{Warum AVR-RISC}
% Da ich mit einem Arduino Entwicklungsboard meinen Einstieg in die Programmierung
% von Mikrocontrollern gefunden habe, und dieses Entwicklungsboard auf den \texttt{ATMega168}
% Mikroprozessor basiert, habe ich mich mehr oder weniger gezwungernermaßen für diese
% Architektur entschieden. Allerdings habe ich ihrer Vorteile kennen und schätzen gelernt
% und mir ist, in der Zeit der Entwicklung des Projekts, immer deutlicher geworden, dass 
% dies die richtige Architektur für diesen Einsatzzweck ist.\\
% In diesem Projekt wird keine besonders starke Rechenkraft vorausgesetzt, so reichen die
% 16Mhz Taktfrequenz vollkommen aus.
\newpage
\section{Arduino - ein freies Entwicklungsboard}
\begin{wrapfigure}{r}{200px}
\begin{center}
\includegraphics[bb=0 0 150 151, scale=0.8]{img/arduino.jpg}
\end{center}
\caption{Arduino Entwicklungsboard}
\label{arduino}
\end{wrapfigure}
Wer effizient mit Mikrocontrollern arbeiten will, benötigt ein
sogenanntes Entwicklungsboard. Dabei handelt es sich um eine
Platine auf welcher der Mikrocontroller sitzt und mit diversen
anderen Bausteinen direkt verbunden ist (die sogenannte Grundbeschaltung). Desweiteren
vereinfacht das Entwicklungsboard das Programmieren des Mikrocontrollers, meist
über die serielle Schnittstelle per ISP (\textbf{I}n-\textbf{S}ystem-\textbf{P}rogramming).\\
\\
Das Arduino Board (siehe Abbildung \ref{arduino}) ist ein solches Entwicklungsboard. Der Mikrocontroller
selbst ist der große, längliche IC (DIP\footnote{Dual in-line package - Gehäuseform} 28-Pin), welcher rechts über den zwei runden, silbernen Kondensatoren auf dem
Board verbaut ist. Der Rest ist die Grundbeschaltung, unter Anderem ein USB-Anschluß und ein Netzanschluß.
%In diesem Projekt handelt es sich um das Arduino NG Rev.C welches als Kernbaustein dient.
%Alle weiteren Schaltungen werden an das Entwicklungsboard angeschlossen, es handelt
%sich quasi um das Herz des Roboters.


%Mit diesem Entwicklungsboard
% habe ich meinen Einstieg in die Mikrcontroller-Welt gemacht und da es außerdem
% noch unter der Creative Commons Lizenz\footnote{Eine OpenSource Lizenz} hergestellt wird, habe ich mich entschieden
% jenes für das Projekt zu verwenden.\\
% \\
% \begin{figure}[h]
% \begin{center}
% \includegraphics[bb=0 0 371 375, scale=0.4]{img/arduino.jpg}
% \end{center}
% \caption{Arduino Entwicklungsboard}
% \label{arduino}
% \end{figure} 




% arduino.jpg: 371x375 pixel, 72dpi, 13.09x13.23 cm, bb=0 0 371 375

% \section{Benötigte Hardware}
% Der SolderBot besteht aus mehreren elektronischen Bausteinen, dazu gehören:
% \begin{itemize}
% \item \textbf{Epson LCD}\\
% Ein \texttt{4x4} cm großer Flüssigkristallbildschirm mit \texttt{131x131} Pixel,
% ausgebaut aus einem alten Nokia 6610 Mobiltelefon.
% \item \textbf{3 x L293 4-channel motor driver}\\
% Dies ist ein IC\footnote{Integrated Circuit - Integrierter Schaltkreis} mit integrierter H-Brücke, welche das schnelle Weschseln von Spannung auf 4 Kanälen, über
% 2 Steuerungskanälen, ermöglicht.
% \item \textbf{3 x Schrittschaltmotor}\\
% Bipolare Schrittschaltmotoren zur präzisen Bewegung der Lötarme, ausgebaut aus
% alten Druckern und Scannern.
% \item \textbf{PS/2 Tastatur}\\
% 105-Tasten Standard Tastatur.
% \item \textbf{Scannergehäuse}\\
% Gehäuse eines alten Scanners.
% \end{itemize}
% \section{Schrittschaltmotor Schaltung}
% \section{LCD Schaltung}

