%
% Copyright 2008  Lukas Kropatschek lukas.krop@gmail.com
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License as
% published by the Free Software Foundation; either version 2 of
% the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
\chapter{Software-Entwicklung für eingebettete Systeme}
In Recheneinheiten von eingebetteten Systemen arbeitet die sogenannte Embedded Software.
Sie enthält die Informationen und Regeln, die zum Steuern des Systems notwendig sind.

\section{Programmiersprachen}
In den 70er Jahren wurde ausschließlich die hardwarenahe Programmiersprache
Assembler eingesetzt. Assembler hat den Vorteil, dass man das Verhalten
des Mikroprozessors genau bestimmen kann, da man direkt Prozessorbefehle im Quellcode festlegt. 
Es leidet aber die Lesbarkeit des Quellcodes darunter, da der
Code, bei entsprechendem Umfang, schnell unübersichtlich wird. Selbst 
der Entwickler könnte nach einiger Zeit Verständnissprobleme seines 
eigenen Codes haben.\\
\\
Später, in den 80er Jahren, wurde teilweise die Programmiersprache C
eingesetzt. Da C eine Hochsprache ist, definiert man im Quellcode
keine einzelnen Prozessorbefehle sondern schreibt den Code in einer
höheren Abstraktionsebene. Der Compiler\footnote{Programm zur Übersetzung von Quellcode zu Maschinencode} 
wandelt den Quellcode dann in einzelne Assemblerbefehle um. Somit ist der Quellcode leichter lesbar.
Außerdem sind Hochsprachen portabler\footnote{Quelltext ist auf mehreren Plattformen verwendbar}, jedoch wird eingebettete 
Software in der Regel für einen bestimmten Zweck geschrieben
weshalb die Portierbarkeit weniger bedeutend ist.\\
\\
Heute werden eingebettete Systeme aufgrund der gewachsenen Leistungsfähigkeit der
Prozessoren beinahe nur noch mit C oder C++\footnote{Erweitertes C zur objektorientierten Programmierung}
programmiert. Hauptsächlich bei zeitkritischen Aufgaben wird auf in C eingebetteten Assembler-Code gesetzt. Natürlich
ist die reine Assembler-Programmierung noch immer möglich.\\
\\
In der Tabelle \ref{asm-vs-c-tbl} werden die wichtigsten Punkte in der Gegenüberstellung von Assembler
und C/C++ kompakt dargestellt. Im weiteren Text werden auf diese Punkte und deren Zusammenhänge untereinander genauer eingegangen.\\
%  \begin{table}
\begin{center}
\begin{tabular}[!h]{ll}
\hline
\rowcolor{gray80}
\textbf{Assembler} & \textbf{C/C++} \\
\rowcolor{white}
% &\\
Bis zu 30\% schnellerer Code & geringfügig langsamerer Code \\
Kryptischer Quelltext & Quelltext ist leserlicher\\
Schlechte Portierbarkeit & Leicht portabel\\
Geringe Abstraktionsebene & Hohe Abstraktionsebene\\
\hline
\end{tabular}
 \captionof{table}{Gegenüberstellung von Assembler und C/C++}
\label{asm-vs-c-tbl}
\end{center}
% \end{table}


\subsection{Assembler}
In Assembler geschriebener Code ist nur innerhalb einer Prozessorfamilie übertragbar,
nirgendwo sonst wäre dieser ausführbar. Der in Assembler geschriebene Quellcode ist bei der
Ausführung zwischen 10 und 30 Prozent schneller als zum Beispiel optimal
kompilierter\footnote{übersetzen des Quellcodes in Maschinencode} C-Code, dies hängt 
allerdings vom eingesetzten Prozessor und Compiler ab.\\
\\
Heute werden mit Assembler hauptsächlich nur noch die sogenannten \textit{Startup}-Module 
programmiert. Diese werden auch oft als \textit{Bootloader} bezeichnet und sind ein Stück
Programm welches für das Laden des Hauptprogramms und das Setzten verschiedener
Parameter (Timer, Watchdog, etc.) verantwortlich ist. Das \textit{Startup}-Modul wird nur beim Systemstart
einmalig ausgeführt.\\
\\
Der folgende Quellcodeausschnitt (Listing \ref{examp-asm}) zeigt, wie man an einem Ausgansport, welcher aus mehreren 
Datenleitungen besteht, alle Datenleitungen auf ein logisches \texttt{HIGH} (dies entspricht, je nach 
Mikrocontroller, der Arbeitsspannung) setzen kann.\\
Zuerst wird mittels \lstinline$.include ``m16def.inc''$ die Definitionsdatei eingebunden. Diese
beinhaltet spezifische Informationen des \texttt{ATMega168}. Dann wird mit \lstinline$ldi r16 0xFF$
das Register 16 mit dem Wert \texttt{0xFF} geladen. Darauf wird der Inhalt des Registers 16
mit \lstinline$out PORTB, r16$ auf \texttt{PORTB} (Das Register für einen Ausgangsport) geschrieben.
Am Ende durchläuft der Mikrocontroller unendlich lang die Schleife die über die Anweisung
\lstinline$end: rjmp end$ definiert wird.
\lstset{language=[x86masm]Assembler, caption=PORTB den Wert 0xFF in Assembler zuweisen,label=examp-asm }
\begin{lstlisting}
.include "m16def.inc"  ; Definitionsdatei einbinden

	ldi r16, 0xFF  ; Register 16 mit Wert 0xFF laden
	out PORTB, r16 ; Wert von Register 16 auf PORTB schreiben

end: rjmp end ; Endlosschleife
\end{lstlisting}

\subsection{C und C++}
\begin{quote}
\textit{``Obwohl B bereits für UNIX entwickelt wurde, stand sie Assembler noch recht nahe. Ihre
prozessorabhängigen Sprachmerkmale und die damit verbundene Unfähigkeit der
Portierung veranlasste Dennis Ritchie 1972, eine portierbare Sprache für
UNIX zu entwickeln. Er nannte sie C.\\
\\
Ende der 70er bis Anfang der 80er Jahre arbeitete Bjarne Stroustrup an einer
Verbesserung der Sprache C: Sie sollte auch die Möglichkeit der Objektorientierung
unterstützen. Es entstand 'C mit Klassen' (C with classes).\\
\\
1983 setzt sich die Bezeichnung C++ durch. Der Name D wurde abgelehnt, weil
er nicht die starke Beziehung zur Sprache C zum Ausdruck brachte.''}
\cite[Willms, 2003, S.14]{cpp-codebook}
\end{quote}
Die Sprache C bietet großen Komfort, da es sich hierbei um eine Hochsprache handelt.
Quellcode der in einer Hochsprache geschrieben wird, ist in der Regel portabel und
für einen Laien leichter zu verstehen. Außerdem ist der Programmfluss einfacher
aus dem Code auslesbar.\\
\\
Da C++ C der Objektorientierung bereichert gibt es bereits diverse Compiler für diese objektorientierte Sprache. Dies ist ein weiterer Komfort, da die objektorientierte
Programmierung bei vielen Problemstellungen einen, im Vergleich zu Assembler, weit
bequemeren Weg zur Lösung bietet.\\
\\
Da es der heutige \textit{``Trend''} ist C/C++ für die Programmierung zu verwenden, habe auch ich
in diesem Projekt fast ausschließlich auf diese Hochsprache(n) gesetzt.\\
\\
Das folgende Beispielprogramm im Listing \ref{examp-c} hat den selben Effekt wie das Beispielprogramm aus Listing \ref{examp-asm}
und soll den Unterschied zwischen der Programmierung in Assembler und C verdeutlichen.
\newpage
\lstset{language=C++, caption=PORTB den Wert 0xFF in C zuweisen,label=examp-c}
\begin{lstlisting}
#include <avr/io.h> // Definitionsdatei laden

int main(void)
{
	PORTB = 0xFF; // PORTB den Wert 0xFF zuweisen
	while(true){} // Endlosschleife
	return 0;
}
\end{lstlisting}



\section{Unterschiede zur Software-Entwicklung am Universal-Rechner}
Bei der Entwicklung von eingebetteter Software gibt es einige Faktoren zu beachten,
welche sich grob von der Software-Entwicklung für den herkömmlichen PC unterscheiden.
\begin{itemize}
\item \textbf{Betriebssystem}
Am herkömmlichen Rechner ist eine Grundsoftware (Betriebssystem) vorhanden, unter welcher
die entwickelten Programme ausgeführt werden. Außerdem organisiert diese den Ablauf mehrerer
Programme welche während des Betriebes geschlossen oder gestartet werden können. Dies ist beim 
Mikrocontroller meist nicht der Fall, das Programm liegt auf einem Festwertspeicher 
ablaufbereit vor und wird ausgeführt und läuft in einer unendlich langen Schleife. 
Es gibt nur ein bestimmtes Programm für einen Zweck.
\item \textbf{Speicher}
Im Mikrocontroller ist ein Festwertspeicher (oft Flash-Speicher) und 
SRAM (\textbf{S}tatic \textbf{R}ead \textbf{a}nd \textbf{M}emory) vorhanden, welche
nicht erweitert werden können. Am herkömmlichen PC kann der Arbeitsspeicher (RAM - \textbf{R}ead \textbf{a}nd \textbf{M}emory)
erweitert werden, außerdem stehen noch Massenspeicher wie z.B. Festplatten oder CD-ROMs zur Verfügung, welche beim Mikrocontroller
wegfallen.
\item \textbf{I/O} Die Ein- und Ausgangsleitungen (\textit{kurz I/O}) sind am Universal-Rechner standardisiert und werden 
im Programm über das zugrunde liegende Betriebssystem angesteuert, wobei beim Mikrocontroller der I/O nicht standardisiert
sondern speziell nach Hard- und Softwaretechnischen Anforderungen vorhanden ist.
\item \textbf{Debugging} Das Debugging\footnote{Fehleruntersuchung von Software} von eingebetteter Software
ist, anders als beim herkömmlichen PC, nicht im Zielsystem\footnote{System in dem die Software ausgeführt werden soll} üblich, da dies
mit großem Aufwand verbunden ist. Es muss ein weiteres System als Debugging-System zwischen Entwicklungssystem\footnote{System in dem die Software entwickelt wird} (manchmal auch \textit{Hostsystem} genannt) und Zielsystem geschalten werden. Am Universal-Rechner steht i.d.R. ein Hochsprachen-Debugger zur Verfügung.
\item \textbf{Selbsttest} Das BIOS (\textbf{B}asic \textbf{I}nput \textbf{O}utput \textbf{S}ystem) führt beim Systemstart am herkömmlichen
PC einen generischen Selbsttest (RAM,Festplatten,etc.) durch, dies muss bei eingebetteter Software als eigenes Modul entwickelt werden.
\end{itemize}
Die Entwicklung sollte aufgrund einiger dieser Anforderungen, möglichst vorausdenkend durchgeführt werden, deshalb ist eine genaue 
Planung meist unabdingbar, dies hängt aber auch vom Umfang des Projekts ab.

\section{Planung mit UML}
Einer der wichtigsten Punkte bei der Entwicklung von Software, vor allem von eingebetteter Software, ist
die Planung. Es muss von Anfang an fest stehen, welche Hardware-Anforderungen gestellt sind
und welche Funktionalität die Software haben soll. Nur so kann man die zu verwendende Hardware richtig und effizient wählen.\\
Wenn klar ist, was die Software später einmal können soll, liegt es nahe ein Diagramm
anzufertigen, welches später bei der Implementierung hilfreich ist.\\
Für diese Diagramme wurde die genormte Form, UML, entwickelt.
\begin{quote}\textit{``Die Unified Modeling Language (UML, engl. Vereinheitlichte Modellierungssprache) 
ist eine von der Object Management Group (OMG) entwickelte und standardisierte Sprache für 
die Modellierung von Software und anderen Systemen.''}
\end{quote}
\cite[Wikimedia, http://de.wikipedia.org/wiki/Unified\_Modeling\_Language, 2. Dez. 2008]{umlwiki}
\\
\\
Mit UML können verschiedenste Diagramme angefertigt werden, die am häufigsten Verwendung finden:
\begin{itemize}
\item{\textbf{Klassendiagramm} - Stellt Klassen, deren Member (Funktionen, Variablen) und deren Zusammenhänge dar.}
\item{\textbf{Komponentendiagramm} - Stellt Softwaremodule als Komponenten und deren Zusammenhänge dar.}
\item{\textbf{Flussdiagramm} - Stellt den Ablauf einer Applikation, in Aufgaben gegliedert dar.}
\end{itemize}
Mit dem quelloffenen Programm Umbrello\footnote{\url{http://uml.sourceforge.net/}} wurde ein Klassendiagramm für dieses Projekt
angefertigt (siehe Anhang E UML-Diagramm). Dieses Diagramm wurde bei der Implementierung des Designs eingesetzt um die Übersicht
zu wahren und so systematisch die geplante Funktionalität zu implementieren.\\
Desweiteren kann man mit Hilfe eines Code-Generators, welcher in Umbrello enthalten ist,
bereits das Grundgerüst, der verschiedenen C++-Klassen automatisch erstellen lassen. Dies
nimmt dem Programmierer einiges an ``Schreibarbeit'' ab und beschleunigt den Entwicklungsprozess.
\section{Entwicklungsumgebung} 
\begin{quote}
\textit{``Eine Programmierumgebung oder \textbf{Entwicklungsumgebung} (engl.: programming environment, development environment)
ist eine Sammlung von Softwarewerkzeugen (engl.: software tool), die zur Entwicklung von prinzipiell beliebiger Software verwendet
werden können.''}\\
\cite[Hansen/Neumann, 2002, S. 964]{wirtschaftsinformatik1}
\end{quote}
Eine Entwicklungsumgebung soll den Programmierer das Erstellen von neuer Software möglichst erleichtern. Die meisten Entwicklungsumgebungen
beinhalten folgende Werkzeuge:
\begin{itemize}
\item Compiler
\item Editor mit Syntaxhervorhebung und automatischer Zeileneinrückung
\item Dokumentationsbrowser
\item Projekt- und Dateiverwaltung
\item Debugger
\item ggf. Assembler
\item ggf. Simulator
\item ggf. Klassengenerator
\item ggf. Terminal-Emulator 
\item ggf. Klassendiagramm 
\item ggf. Klassenübersicht
\end{itemize}
In Abbildung \ref{ide-kdev} ist die Integrierte Entwicklungsumgebung KDevelop\footnote{\url{http://www.kdevelop.org}} zu sehen.\\
\\
Für die Entwicklung von Anwendungen für den Mikrocontroller wird ein \textit{Entwicklungssystem} benötigt, welches wie folgt
definiert ist.
\begin{quote}
\textit{``Entwicklungssysteme bestehen aus einer auf einem PC ablaufenden Software und aus einer Hardware, die den 
Controller mit einer Programmiereinrichtung verbindet.''}\\
\cite[Schmitt, 2008, S. 46]{Schmitt2008}
\end{quote}
So wird zur Entwicklungsumgebung zusätzlich eine Hardware benötigt, die eine Verbindung zwischen Mikrocontroller
und Computer ermöglicht, um den Controller zu programmieren. Hier kommt das Entwicklungsboard Arduino zum Einsatz,
welches genau für diesen Zweck geschaffen wurde (siehe Kapitel 2.3).
\begin{figure}[h]
\begin{center}
\includegraphics[bb=0 0 600 499, scale=0.9]{img/ide.jpg}
\end{center}
\caption{KDevelop: Integrierte Entwicklungsumgebung}
\label{ide-kdev}
\end{figure} 
