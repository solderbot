%
% Copyright 2008  Lukas Kropatschek lukas.krop@gmail.com
%
% This program is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public License as
% published by the Free Software Foundation; either version 2 of
% the License, or (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
\chapter{Embedded Systems}
Den Begriff ``Embedded Systems'' oder ``eingebettete Systeme'' gibt es seit den 90er Jahren, allerdings waren
diese Art von Systemen, wenngleich auch in minimal anderer Form, schon davor präsent. Sie wurden dem Begriff
der ``Mikroprozessortechnik'' untergeordnet. Diese Systeme gibt es schon seit den 70er Jahren des vorigen Jahrhunderts.
\cite[vgl. Wüst, 2006, S. 1-3]{wuest2006}

\section{Definition}
Die meisten Menschen könnten ohne eingebettete Systeme (engl. \textbf{\textit{Embedded systems}}) wohl 
kaum mehr leben, wissen aber von deren Existenz meist nichts. Diese speziellen Systeme sind 
in vielen Geräten zu finden, die wir täglich verwenden, wie z.B. Autos (ABS, Navigationssystem), 
(Mobil-)Telefonen, MP3-Playern, Spielwaren, Sicherheitssystemen, Herzschrittmachern, 
Digitaluhren, Fernsehgeräten oder Flugzeugen. Sogar so mancher \textit{``Hightech-Toaster''} wird
von einem eingebetteten System gesteuert.\\
In dieser Arbeit ist der Begriff \textit{eingebettete Systeme} so zu verstehen:
\begin{quote}\textit{``Eingebettet Systeme sind informationsverarbeitende Systeme, die in ein 
größeres Produkt integriert sind, und die normalerweise nicht direkt vom Benutzer 
wahrgenommen werden.''} \cite[Marwedel, 2008, S. 1]{ES}
\end{quote}

\subsection{Gemeinsame Merkmale}
Diese System haben diverse gemeinsame Merkmale. Eingebettete Systeme...
\begin{itemize}
\item ...dienen zur \textbf{Steuerung} von technischen Anlagen.
\item ...\textbf{interagieren mit der Umwelt}. Informationen werden durch Sensoren aufgenommen oder sie beeinflussen die Umwelt mit 
Aktuatoren\footnote{Geräte, welche eine numerische Größe in eine physikalische Größe umwandeln}.
\item ...haben \textbf{keine} \textit{``übliche''} Benutzerschnittstelle wie Monitore, Tastatur oder Maus. Stattdessen
kommen kleine Bildschirme, Steuerknöpfe oder Tastenfelder zum Einsatz.
\item ...werden für \textbf{eine spezielle Aufgabe} entwickelt. Zum Beispiel wird ein Prozessor im Steuergerät eines Zuges oder Autos
immer nur genau seine Steuerungssoftware ausführen. Niemals würde jemand versuchen ein Tabellenkalkulationsprogramm
oder andere Software auf solchen Systemen zu betreiben. Dies hat folgenden Gründe:
\begin{itemize}
\item Durch die Ausführung von anderen Programmen auf diesen Systemen, würden sie dadurch weniger \textbf{verlässlich}.
\item Andere Programme benötigen Ressourcen, diese sind in \textbf{effizient} entwickelten eingebetteten Systemen
allerdings nicht vorhanden.
\end{itemize}
\item ...müssen \textbf{verlässlich} und \textbf{effizient} sein.
\end{itemize}


\subsection{Verlässlichkeit}
%Es gilt den letzten Punkt besonders hervorzuheben. 
Da viele eingebettet Systeme zeitkritische
Berechnungen durchzuführen haben ist es sehr wichtig, dass diese Systeme äußerst verlässlich sind.
Solche Systeme dienen häufig der Steuerung von Sicherheitssystemen in Transportmitteln o.Ä. (Flugzeug, Auto).
Wenn diese Berechnungen nicht im vorgesehenen Zeitrahmen erfolgen könnten Menschen zu Schaden kommen. In einem 
solchen Fall spricht man von \textbf{harten Echtzeitbedingungen}. Wenn das Nichteinhalten der
vorgesehenen Echtzeitbedingung nicht gefährlich ist, spricht man von \textbf{weichen Echtzeitbedingungen}.\cite[vgl. Marwedel, 2008, S.4]{ES}
%In diesem Projekt können glücklicherweise keine Menschen zu Schaden kommen, außerdem treten kaum zeitkritische Berechnungen auf.

\subsection{Effizienz}
Bei der Entwicklung von eingebetteten Systemen ist es äußerst wichtig auf die Effizienz des Systems
zu achten. Die folgenden Faktoren können dazu verwendet werden, die Effizienz von eingebetteten Systemen
zu beschreiben:
\begin{itemize} 
\item \textbf{Preis:} Die Hardware sollte möglichst günstig und gut verfügbar sein (Massenproduktion).
\item \textbf{Energie:} Da viele eingebettete Systeme mobil sind beziehen sie ihre Energie aus Batterien oder Akkumulatoren. Um einen längeren 
Betrieb zu gewährleisten muss der Energieverbrauch möglichst gering gehalten werden.
\item \textbf{Gewicht und Platz:} Auch hier gilt wieder: je geringer das Gewicht und der Platzverbrauch desto 
besser eignet sich ein Produkt für den mobilen Einsatz.
\item \textbf{Codegröße:} Platz spielt in eingebetteten Systemen eine große Rolle. Der Quellcode für die Anwendung muss im Mikrocontroller 
selbst gespeichert werden, da es (üblicherweise) keinen Zugriff auf Massenspeichergeräte gibt. Daher muss der Quellcode sehr 
effizient geschrieben werden. %vergleiche bla bla
\end{itemize}
In eingebetteten Systemen soll nur die nötigste Hardware eingesetzt werden, dafür aber sollte diese komplett ausgeschöpft werden.
Dies gewährleistet einen minimalen Platzverbrauch.
%In diesem Projekt ist ausreichend Platz vorhanden, außerdem handelt
% es sich um ein Hobbyprojekt und somit stehen mir auch nicht die nötigen Gerätschaften für die Herstellung (von z.B. elektronischen
% Schaltungen) auf kleinstem Raum, zur Verfügung. 
% Jedoch ist die eingesetzte Hardware genau auf dieses Projekt abgestimmt und wird voll ausgeschöpft. Beispielsweise
% belegt der Quellcode beinahe 95\% des zur Verfügung stehenden Speichers.

\section{Historische Entwicklung}
Die ersten Schritte am Sektor der eingebetteten System haben im Bereich der Raumfahrt stattgefunden. Hier wurden
verschiedenen Navigations- und Leitsysteme für Raketen und das Apollo-Programm (\textit{AGC - Apollo Guidance Computer} siehe 
Abbildung \ref{agc}) entwickelt.
\begin{figure}[h!]
\begin{center}
\includegraphics[bb=0 0 600 320, scale=0.5]{img/agc.jpg}
\end{center}
\caption{Apollo Guidance Computer \textit{Quelle:Wikipedia}}
\label{agc}
\end{figure} 
Nach den ersten Anwendungen in den 1960er Jahren ist der Preis für Mikroprozessoren stark gesunken und die 
Rechenkraft ist gestiegen. Der erste Mikroprozessor, der \texttt{Intel 4004}, wurde zum Beispiel für den Einsatz in
Taschenrechnern und ähnlichen kleinen Systemen hergestellt. Jedoch benötigte dieser noch einen relativ großen Anteil an
Zusatzchips und Schaltungen. Als die Kosten für die Mikrocontroller und Mikroprozessoren weiter gesunken sind, wurden viele analoge Schaltungen
durch eingebettete Systeme ersetzt. In der Mitte der 80er Jahre wurden viele externe Komponenten in den 
Mikrocontroller integriert und diese \textit{``neue''} Form der Mikrocontroller hat viele aufwändige Schaltungen
mit nur einem Chip ersetzt.\cite[vgl. Schäffer, 2008, S. 11-13]{Schaeffer2008}\\
\\
Heute finden wir eingebettete Systeme in beinahe allen elektronischen Geräten, von denen der durchschnittliche 
US-Amerikaner, laut einer Studie im Jahre 1996, bis zu 60 mal täglich Gebrauch macht. \cite[vgl. Camposano/Wolf, 1996]{msg-editors-in-chief} Wenn die Waschmaschine schleudert, das Mobiltelefon 
läutet oder die Türen des Eisenbahnwaggons sich automatisch öffnen, wissen die meisten nicht, dass gerade
ein eingebettetes System hier die Arbeit steuert.
In Abbildung \ref{intel-atmega} sind der erste Mikroprozessor \texttt{Intel 4004} und der in diesem Projekt verwendete
Atmel\copyright ~AVR \texttt{ATMega168} abgebildet. An den \textit{``Füßchen''} (Ein-/Ausgangsleitungen) der Controller
kann man schon erkennen, dass im \texttt{ATMega168} wesentlich mehr (ehemalig externe) Komponenten
untergebracht sind.
\begin{figure}[h]
\begin{center}
\includegraphics[bb=0 0 600 253, scale=0.5]{img/intel-mega.jpg}
\end{center}
\caption{Links Intel 4004 (\texttt{2,54x0.76cm}) und rechts AVR ATMega168 (\texttt{4.06x0.76cm})}
\label{intel-atmega}
\end{figure} 

\section{Anwendungsbereiche}
Folgende Auflistung fasst nun die wichtigsten Anwendungsbereiche für eingebettet Systeme zusammen:
\cite[vgl. Marwedel, 2008, S. 6-8]{ES}
\begin{itemize}
\item \textbf{Automobilindustrie:} Einrichtungen wie das ESP, ABS, Airbag-Steuerung oder das Navigationssystem, welche in jedem Neuwagen aufzufinden sind, 
werden von eingebetteten System gesteuert.
\item \textbf{Flugzeugtechnik} Jeder hat bereits den Innenraum eines Flugzeugcockpits gesehen, gespickt voll mit Messgeräten und anderer Elektronik. Die meisten
Aufgaben in einem Flugzeug übernehmen eingebettete Systeme, diese müssen äußerst Zuverlässig sein, da im Falle eines Ausfalls möglicherweise Menschen zu Schaden
kommen könnten.
\item \textbf{Eisenbahntechnik:}
Wann welche Weiche gestellt werden muss, entscheidet längst nicht mehr der Mensch, außer in Ausnahmesituationen. All dies wird bereits von eingebetteten
Sicherheitssystemen geregelt. Auch hier ist das erwünschte Verhalten der Systeme von großer Wichtigkeit.
\item \textbf{Telekommunikation:} Wer hat noch kein Handy gehabt? Die meisten Menschen haben bereits mehrere Mobiltelefone besessen und der
Markt scheint nicht schlecht zugehen. In jedem Handy arbeitete ein eingebettetes System und die Funktionalität (Multimediafähigkeit) dieser
Telefone steigt stetig.
\item \textbf{Unterhaltungselektronik:} Video- und Audiosysteme sind ein erheblicher Part der Elektronikindustrie. Auch hier ist der Trend
bezüglich Qualität und Umfang stark steigend. Durch immer bessere Methoden werden diese Eigenschaften verbessert und auch da kommt in jedem
DVD-Player und jeder Spielekonsole ein eingebettetes System zum Einsatz.
\item \textbf{Medizinische Systeme:} Mit Hilfe eingebetteter Systeme werden medizinische Geräte hergestellt mit denen man Krankheiten erkennen und
Untersuchungen durchführen kann die ohne diesen informationsverarbeitenden Systemen schwerst bis unmöglich wären.
\item \textbf{Militärische Systeme:} In vielen Raketen sind eingebettete Systeme zur Zielfindung im Einsatz, aber auch für Kommunikation und Aufklärungsarbeiten
werden solche Systeme eingesetzt.
\item \textbf{Fabrikensteuerung:} Das CAM (\textbf{C}omputer \textbf{A}ided \textbf{M}anufacturing) wäre ohne eingebettete Systeme nicht möglich. Diese erlauben
eine hohe Präzision bei verschiedensten Fabrikationsvorgängen. Der in diesem Projekt konstruierte Roboter fällt auch unter diese Kategorie. Er ist zwar längst nicht
so präzise wie Geräte zur Herstellung von Prozessoren und ist auch nicht zur Massenproduktion geeignet aber das Prinzip ist gleich. Arbeitserleichterung und Beschleunigung
des Arbeitsvorgangs. Allerdings erledigen eingebettete Systeme auch die Steuerung von zum Beispiel Sicherheitsanlagen in Atomkraftwerken oder die einfache Steuerung eines Ventils.
\item \textbf{Intelligente Gebäude:} Nach der Sonneneinstrahlung automatisch bewegete Rollos, automatisch öffnende Türen oder Systeme zur Verringerung des Energieverbrauchs werden
in modernen Haushalten eingesetzt. Gesteuert werden diese von eingebetteten Systemen.
\item \textbf{Robotik:} Arbeitsroboter wie sie in Fabriken eingesetzt werden oder aber auch menschenähnliche Roboter werden von eingebetteten Systemen gesteuert. Sie bilden praktisch das
Hirn dieser Roboter.
\item \textbf{Authentifizierungs-Systeme:} Großes Potential liegt natürlich auch in der Sicherheitstechnik. So können zum Beispiel zur Authentifizierung Fingerabdrucksensoren
oder Gesichtserkennungssysteme eingesetzt werden. Ein besonders interessantes Beispiel ist auch der sogenannte SMARTpen\textregistered. Auf dieses Beispiel werde ich nun näher
in der nächsten Sektion eingehen.
\end{itemize}



\section{Beispiel: SMARTpen\textregistered}
Dieser Stift (Abbildung \ref{spen}) dient als biometrisches Messgerät, welches Schreibdynamiken misst und diese mit Referenzdaten 
zur eindeutigen Identifikation eines Benutzers abgleicht. Während mit dem SMARTpen zum Beispiel eine Unterschrift
geschrieben wird, messen die Sensoren, welche in den Stift integriert sind, diverse biometrische Beschaffenheiten aus.\\
Folgende Werte werden ausgemessen:
\begin{itemize}
\item Neigung
\item Schreibgeschwindigkeit
\item Druck
\end{itemize}
Diese drei Faktoren sind bei jedem Menschen zumindest geringfügig unterschiedlich. Die feinen Sensoren im Stift messen diese Faktoren
und senden sie sofort an einen Empfänger, zum Beispiel ein PC, auf welchem bereits Daten zum Abgleich vorhanden sind.\\
Mit einer sehr geringen Toleranzgrenze kann das System einen Benutzer eindeutig identifizieren und zuordnen.
So können zum Beispiel sicher Interneteinkäufe oder Geldtransaktionen ausgeführt werden. Dies würde
klassische Methoden ablösen.
%Dieser Stift hat ein enorm großes Potential ist aber noch nicht verbreitet.
\begin{center}
\begin{figure}[h]
\includegraphics[bb=0 0 384 132]{img/smartpen.jpg}
\caption{SMARTpen Schema}
\label{spen}
\end{figure} 
\end{center}
\begin{quote}\textit{``Der LCI-SMARTpen ist das Ergebnis einer Zusammenarbeit zwischen LCI, Niederlande und IMEC, Belgien. Das 
Team löste das schwierige Miniaturisierungsproblem, Computer-Komponenten wie Sensoren, Maus, digitaler Signalprozessor, 
Sender/Empfänger und Verschlüsselungssoftware so klein zu bauen, dass sie in einen Kugelschreiber passen. Der LCI-SMARTpen 
ist eine rein europäische Erfindung.''}
\cite[Detlef Höffken, http://cordis.europa.eu/esprit/src/eitcp4de.htm, 16. März 2006]{smartpen}
\end{quote}
Ich finde besonders die Evolution von eingebetteten Systemen im Bereich der Sicherheitstechnik interessant
da es hier viel Potential gibt. Ich habe diesen Stift herangezogen, da er meiner Meinung nach ein gutes Beispiel für ein
innovatives und effizientes Produkt ist. Es wird zum Beispiel der Platz sehr effizient genützt, da es schwierig ist die Hardware-Komponenten
in einem Stift unterzugbringen.
